﻿using System;
using System.Text;

namespace HTools
{
    /// <summary>
    /// Decimal (base10) conversion
    /// </summary>
    public class Dec
    {
        #region [ Dec To Bin ]
        /// <summary>
        /// Int to binary string.
        /// </summary>
        /// <param name="dec">The dec.</param>
        /// <returns></returns>
        public static string ToBin(int dec)
        {
            return ToBin(dec, 0);
        }

        /// <summary>
        /// Decimal to the binary.
        /// </summary>
        /// <param name="dec">The decimal (signed accepted).</param>
        /// <param name="totalLength">The total length, pad the left of the binary with zero's.</param>
        /// <returns></returns>
        public static string ToBin(int dec, int totalLength)
        {
            UInt32 udec = TwoComplement32Bit(dec);

            string bin = "";
            for (int i = 0; i < 32; i++)
            {
                string bit = (udec & 0x1).ToString();
                bin = bit + bin;
                udec >>= 1;
            }

            return Str.PadLeft(bin, totalLength, '0');
        }

        /// <summary>
        /// Long to binary string.
        /// </summary>
        /// <param name="dec">The dec.</param>
        /// <returns></returns>
        public static string ToBin(long dec)
        {
            return ToBin(dec, 0);
        }

        /// <summary>
        /// Decimal to the binary.
        /// </summary>
        /// <param name="dec">The decimal (signed accepted).</param>
        /// <param name="totalLength">The total length, pad the left of the binary with zero's.</param>
        /// <returns></returns>
        public static string ToBin(long dec, int totalLength)
        {
            ulong udec = TwoComplement64Bit(dec);

            string bin = "";
            for (int i = 0; i < 64; i++)
            {
                string bit = (udec & 0x1).ToString();
                bin = bit + bin;
                udec >>= 1;
            }

            return Str.PadLeft(bin, totalLength, '0');
        }
        #endregion

        #region [ Dec To Hex ]
        /// <summary>
        /// Convert int into hex.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static string ToHex(int value)
        {
            return ToHex(value, 0);
        }

        /// <summary>
        /// Convert int into hex. Pad zero to the left of the hex satisfy 
        /// the total length specified (number of chars).
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="totalLength">The total length.</param>
        /// <returns></returns>
        public static string ToHex(int value, int totalLength)
        {
            UInt32 uvalue = TwoComplement32Bit(value);
            const string STR_Hex = "0123456789ABCDEF";

            string hex = "";
            // 32bit so we need 32/8 = 4bytes x2 => 8hex chars
            for (int i = 8; i > 0; i--)
            {
                int letter = (int)(uvalue & 0xF);
                hex = STR_Hex[letter].ToString() + hex;
                uvalue >>= 4;
            }

            return Str.PadLeft(hex, totalLength, '0');   
        }

        private static UInt32 TwoComplement32Bit(int value)
        {
            UInt32 uvalue;
            if (value < 0)
            {
                // -32 => 0xE0
                // 1. Represent binary in binary, 00100000 (the abs value)
                // 2. Reverse the bits, 11011111 (this is XOR)
                // 3. Add 1, 11100000
                // 4. The result is a 2' complement (negative representaiton)
                value *= -1;
                uvalue = (UInt32)~value; // xor
                uvalue += 1;
            }
            else
            {
                uvalue = (UInt32)value;
            }
            return uvalue;
        }

        /// <summary>
        /// Convert long into hex.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static string ToHex(long value)
        {
            return ToHex(value, 0);
        }

        /// <summary>
        /// Convert long into hex. Pad zero to the left of the hex satisfy 
        /// the total length specified (number of chars).
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="totalLength">The total length.</param>
        /// <returns></returns>
        public static string ToHex(long value, int totalLength)
        {
            ulong uvalue = TwoComplement64Bit(value);
            const string STR_Hex = "0123456789ABCDEF";

            string hex = "";
            // 64bit so we need 64/8 = 8bytes x2 => 16hex chars
            for (int i = 16; i > 0; i--)
            {
                int letter = (int)(uvalue & 0xF);
                hex = STR_Hex[letter].ToString() + hex;
                uvalue >>= 4;
            }

            return Str.PadLeft(hex, totalLength, '0');   
        }

        private static ulong TwoComplement64Bit(long value)
        {
            ulong uvalue;
            if (value < 0)
            {
                // -32 => 0xE0
                // 1. Represent binary in binary, 00100000 (the abs value)
                // 2. Reverse the bits, 11011111 (this is XOR)
                // 3. Add 1, 11100000
                // 4. The result is a 2' complement (negative representaiton)
                value *= -1;
                uvalue = (ulong)~value; // xor
                uvalue += 1;
            }
            else
            {
                uvalue = (ulong)value;
            }
            return uvalue;
        }
        #endregion

        #region [ Dec To Bytes ]
        /// <summary>
        /// Convert an int into an array of bytes.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static byte[] ToBytes(int value)
        {
            byte[] bytes = Dec.ToBytes(value, 0);
            return bytes;
        }

        /// <summary>
        /// Convert an int into an array of bytes 
        /// padded zero's to the left to fill the byte count.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="byteCount">The total byte count to return.</param>
        /// <returns></returns>
        public static byte[] ToBytes(int value, int totalLength)
        {
            return Dec.ToBytes((long)value, totalLength);
        }

        /// <summary>
        /// Convert a long value into an array of bytes.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static byte[] ToBytes(long value)
        {
            byte[] bytes = Hex.ToBytes(Dec.ToHex(value));
            return bytes;
        }

        /// <summary>
        /// Convert an long into an array of bytes 
        /// padded zero's to the left to fill the byte count.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="byteCount">The total byte count to return.</param>
        /// <returns></returns>
        public static byte[] ToBytes(long value, int totalLength)
        {
            string hex = Dec.ToHex(value);
            byte[] bytes = Hex.ToBytes(hex, totalLength);
            return bytes;
        }
        #endregion
    }
}
