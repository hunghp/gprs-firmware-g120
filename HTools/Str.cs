﻿using System;
using System.Text;

namespace HTools
{
    #region [ Enum ]
    /// <summary>
    /// Case sensitivity
    /// </summary>
    public enum CaseTreatment
    {
        /// <summary>
        /// Case sensitive
        /// </summary>
        CaseSensitive,

        /// <summary>
        /// Case insensitive
        /// </summary>
        CaseNotSesitive
    }
    #endregion

    /// <summary>
    /// String manipulation and validation methods.
    /// </summary>
    public class Str
    {        
        #region [ Number Detection ]
        /// <summary>
        /// Check if the string is a valid integer
        /// </summary>
        /// <param name="num">The num.</param>
        /// <returns>
        /// 	<c>true</c> if the specified num is integer; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsInteger(string num)
        {
            num = Trim(num);

            if (IsEmpty(num))
            {
                return false;
            }

            if (num[0] == '-' || num[0] == '+')
            {
                num = num.Substring(1);
            }

            if (num.Length == 0)
            {
                return false;
            }

            for (int i = 0; i < num.Length; i++)
            {
                if (!Chr.IsAsciiDigit(num[i]))
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Check to see the string is a valid double format (Valid: -103.3, +23.8, 3.23, 8))
        /// </summary>
        /// <param name="num">The number.</param>
        /// <returns>
        /// 	<c>true</c> if the specified num is double; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsDouble(string num)
        {
            num = Str.Trim(num);

            if (IsEmpty(num))
            {
                return false;
            }

            if (num[0] == '-' || num[0] == '+')
            {
                num = num.Substring(1);
            }

            if (num.Length == 0)
            {
                return false;
            }

            bool hasOneDot = false;
            for (int i = 0; i < num.Length; i++)
            {
                if (!Chr.IsAsciiDigit(num[i]) && num[i] != '.')
                {
                    return false;
                }

                if (num[i] == '.')
                {
                    if (hasOneDot)
                    {
                        return false;
                    }
                    hasOneDot = true;
                }
            }
            return true;
        }
        #endregion

        #region [ IsEmpty ]
        /// <summary>
        /// Determine if the string is empty (null or filled with space)
        /// </summary>
        /// <param name="txt"></param>
        /// <returns></returns>
        public static bool IsEmpty(string txt)
        {
            if (txt == null)
            {
                return true;
            }
            return txt.Trim().Length == 0;
        }

        /// <summary>
        /// Determines whether the string has something other than space.
        /// </summary>
        /// <param name="txt">The TXT.</param>
        /// <returns>
        /// 	<c>true</c> if the string is not empty; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsNotEmpty(string txt)
        {
            return !IsEmpty(txt);
        }
        #endregion

        #region [ Is Equal ]
        /// <summary>
        /// Case insensitive string comparision.
        /// </summary>
        /// <param name="a">First string.</param>
        /// <param name="b">Second string.</param>
        /// <returns>
        /// 	<c>true</c> if the specified a is equal to b; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsEqual(string a, string b)
        {
            return IsEqual(a, b, CaseTreatment.CaseNotSesitive);
        }

        /// <summary>
        /// Compare the two strings, null value accepted.
        /// </summary>
        /// <param name="a">The first string</param>
        /// <param name="b">The second string.</param>
        /// <param name="c">The case sensitivity option.</param>
        /// <returns>
        /// 	<c>true</c> if the a equal to b; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsEqual(string a, string b, CaseTreatment c)
        {
            if (a == null && b == null)
            {
                return true;
            }
            if (a == null || b == null)
            {
                return false;
            }

            if (c == CaseTreatment.CaseSensitive)
            {
                return a.Equals(b);
            }
            return a.ToUpper().Equals(b.ToUpper());
        }

        /// <summary>
        /// Case insensitive string comparision
        /// </summary>
        /// <param name="a">First string.</param>
        /// <param name="b">Second string.</param>
        /// <returns>
        /// 	<c>true</c> if the specified a is equal to b; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsNotEqual(string a, string b)
        {
            return IsNotEqual(a, b, CaseTreatment.CaseNotSesitive);
        }

        /// <summary>
        /// Determines whether the two string is not equal to each other.
        /// </summary>
        /// <param name="a">First string.</param>
        /// <param name="b">Second string.</param>
        /// <param name="c">Case treatment.</param>
        /// <returns></returns>
        public static bool IsNotEqual(string a, string b, CaseTreatment c)
        {
            return !IsEqual(a, b, c);
        }
        #endregion

        #region [ Trim ]
        /// <summary>
        /// Trim the string, if "s" is null then return String.Empty
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string Trim(string s)
        {
            return Trim(s, string.Empty);
        }

        /// <summary>
        /// Trim the string, if "s" is null then return valueOnNull
        /// </summary>
        /// <param name="s"></param>
        /// <param name="valueOnNull"></param>
        /// <returns></returns>
        public static string Trim(string s, string valueOnNull)
        {
            if (s == null)
            {
                return valueOnNull;
            }
            return s.Trim();
        }

        /// <summary>
        /// Trim the string before compare
        /// </summary>
        /// <param name="a">First string.</param>
        /// <param name="b">Second string.</param>
        /// <param name="c">What to use for the case sensitivity.</param>
        /// <returns></returns>
        public static bool TrimEqual(string a, string b, CaseTreatment c)
        {
            a = Trim(a, null);
            b = Trim(b, null);

            return IsEqual(a, b, c);
        }

        /// <summary>
        /// Trim the string before compare (case insensitive)
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool TrimEqual(string a, string b)
        {
            return TrimEqual(a, b, CaseTreatment.CaseNotSesitive);
        }

        /// <summary>
        /// Trim (space, tab, newline, carrage return, null, vertical tab)
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string TrimAggressive(string str)
        {
            //                              Space,       Tab,       NewLine,     CR,          Null,     VerTab
            return str.Trim(new char[] { (char)0x20, (char)0x09, (char)0x0A, (char)0x0D, (char)0x00, (char)0x0B });
        }
        #endregion

        #region [ Contains ]
        /// <summary>
        /// Determines whether the haystack contains the needle (ignore case).
        /// </summary>
        /// <param name="haystack">The haystack.</param>
        /// <param name="needle">The needle.</param>
        /// <returns>
        /// 	<c>true</c> if found the needle in the haystack; otherwise, <c>false</c>.
        /// </returns>
        public static bool Contains(string haystack, string needle)
        {
            return Contains(haystack, needle, true);
        }

        /// <summary>
        /// Check to see haystack contains the needle.
        /// </summary>
        /// <param name="haystack">The haystack.</param>
        /// <param name="needle">The needle.</param>
        /// <param name="ignoreCase">if set to <c>true</c> to ignore the case.</param>
        /// <returns>
        /// 	<c>true</c> if found the needle in the haystack; otherwise, <c>false</c>.
        /// </returns>
        public static bool Contains(string haystack, string needle, bool ignoreCase)
        {
            if (ignoreCase)
            {
                return haystack.ToUpper().IndexOf(needle.ToUpper()) > -1;
            }

            return haystack.IndexOf(needle) > -1;
        }

        /// <summary>
        /// Check to see the haystack contains all the needles (ignore case)
        /// </summary>
        /// <param name="haystack">The haystack.</param>
        /// <param name="needles">The needles.</param>
        /// <returns>
        /// 	<c>true</c> if found the all the needles in the haystack; otherwise, <c>false</c>.
        /// </returns>
        public static bool ContainsAll(string haystack, params string[] needles)
        {
            return ContainsAll(haystack, true, needles);
        }

        /// <summary>
        /// Check to see the haystack contains all the needles .
        /// </summary>
        /// <param name="haystack">The haystack.</param>
        /// <param name="ignoreCase">if set to <c>true</c> ignore case.</param>
        /// <param name="needles">The needles.</param>
        /// <returns>
        /// 	<c>true</c> if found the all the needles in the haystack; otherwise, <c>false</c>.
        /// </returns>
        public static bool ContainsAll(string haystack, bool ignoreCase, params string[] needles)
        {
            foreach (string needle in needles)
            {
                if (!Contains(haystack, needle, ignoreCase))
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Check to see the haystack contains ANY of the needles (ignore case).
        /// </summary>
        /// <param name="haystack">The haystack.</param>
        /// <param name="needles">The needles.</param>
        /// <returns>
        /// 	<c>true</c> if the specified haystack contains any; otherwise, <c>false</c>.
        /// </returns>
        public static bool ContainsAny(string haystack, params string[] needles)
        {
            return ContainsAny(haystack, true, needles);
        }

        /// <summary>
        /// Check to see the haystack contains ANY of the needles.
        /// </summary>
        /// <param name="haystack">The haystack.</param>
        /// <param name="ignoreCase">if set to <c>true</c> ignore case.</param>
        /// <param name="needles">The needles.</param>
        /// <returns>
        /// 	<c>true</c> if the specified haystack contains any; otherwise, <c>false</c>.
        /// </returns>
        public static bool ContainsAny(string haystack, bool ignoreCase, params string[] needles)
        {
            foreach (string needle in needles)
            {
                if (Contains(haystack, needle, ignoreCase))
                {
                    return true;
                }
            }
            return false;
        }
        #endregion

        #region [ Others ]
        /// <summary>
        /// Reverses the string.
        /// </summary>
        /// <param name="s">The string.</param>
        /// <returns></returns>
        public static string Reverse(string str)
        {
            char[] arr = str.ToCharArray();
            char[] newArr = new char[arr.Length];

            int len = arr.Length - 1;
            for (int i = len; i < 0; i--)
            {
                newArr[len - i] = arr[i];
            }

            return new string(newArr);
        }
        #endregion

        /// <summary>
        /// If the string is longer than the specified length 
        /// then return the left portion of the input.  If the input is 
        /// less than the specified length then the input is return as is.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="length">The length.</param>
        /// <returns>Return emptied string if the input is null.</returns>
        public static string CutLeft(string input, int length)
        {
            if (input == null)
            {
                return "";
            }

            if (input.Length <= length)
            {
                return input;
            }

            return input.Substring(0, length);
        }

        /// <summary>
        /// If the string is longer than the specified length 
        /// then return the right portion of the input.  If the input is 
        /// less than the specified length then the input is return as is.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="length">The length.</param>
        /// <returns>Return emptied string if the input is null.</returns>
        public static string CutRight(string input, int length)
        {
            if (input == null)
            {
                return "";
            }

            if (input.Length <= length)
            {
                return input;
            }

            int offset = input.Length - length;
            return input.Substring(offset);
        }

        /// <summary>
        /// Extended version of String.SubString(). 
        /// This allow negative index (which is taking the substring from the end).
        /// </summary>
        /// <example>Str.SubString("Hello", -3) => return "llo"</example>
        /// <param name="input">The input.</param>
        /// <param name="legth">The legth.</param>
        /// <returns></returns>
        public static string SubString(string input, int index)
        {
            if (index > 0)
            {
                return input.Substring(index);
            }

            return CutRight(input, -index);
        }

        #region [ IntVal ]
        /// <summary>
        /// Evaluate and extract the integer from the string
        /// </summary>
        /// <param name="txt">The string.</param>
        /// <returns></returns>
        public static int IntVal(string txt)
        {
            return IntVal(txt, 0);
        }

        /// <summary>
        /// Try to get a number from a string, if invalid return defaultValue.
        /// </summary>
        /// <param name="txt">The string.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        public static int IntVal(string txt, int defaultValue)
        {
            if (txt.Trim().Length == 0)
            {
                return defaultValue;
            }

            try
            {
                if (!Str.IsInteger(txt))
                {
                    return defaultValue;
                }

                int i;
                try
                {
                    i = Convert.ToInt32(txt);
                    return i;
                }
                catch { }

                return defaultValue;
            }
            catch
            {
                return defaultValue;
            }
        }

        /// <summary>
        /// Convert boolean into bit representation
        /// </summary>
        /// <param name="b">Boolean value</param>
        /// <returns></returns>
        public static int IntVal(bool b)
        {
            return b ? 1 : 0;
        }
        #endregion

        #region [ DoubleVal ]
        /// <summary>
        /// Extract double value from a string (accept currency formatting).
        /// </summary>
        /// <param name="txt">The text.</param>
        /// <returns>Return "0" on error.</returns>
        public static double DoubleVal(string txt)
        {
            return DoubleVal(txt, 0);
        }

        /// <summary>
        /// Extract double value from the string (accept currency formatting)
        /// </summary>
        /// <param name="txt">The text.</param>
        /// <param name="defaultValue">The default value to return on error.</param>
        /// <returns></returns>
        public static double DoubleVal(string txt, double defaultValue)
        {
            double result;
            try
            {
                result = Convert.ToDouble(txt);
                return result;
            }
            catch { }

            return defaultValue;
        }

        /// <summary>
        /// Convert boolean into bit representation
        /// </summary>
        /// <param name="b">Boolean value</param>
        /// <returns></returns>
        public static double DoubleVal(bool b)
        {
            return b ? 1 : 0;
        }
        #endregion        

        /// <summary>
        /// Find and replace (case sensitive).
        /// </summary>
        /// <param name="str">The content.</param>
        /// <param name="oldValue">The old value.</param>
        /// <param name="newValue">The new value.</param>
        /// <returns></returns>
        public static string Replace(string str, string oldValue, string newValue)
        {
            return Replace(str, oldValue, newValue, false);
        }


        public static string Replace(string str, string oldValue, string newValue, bool ignoreCase)
        {
            string compareString = ignoreCase ? str.ToUpper() : str;
            string searchValue = ignoreCase ? oldValue.ToUpper() : oldValue;

            int foundIndex = compareString.IndexOf(searchValue);
            if (foundIndex < 0)
            {
                return str;
            }

            int lastPlace = foundIndex + searchValue.Length;
            string result = str.Substring(0, foundIndex) + newValue;

            while ((foundIndex = compareString.IndexOf(searchValue, lastPlace)) >= 0)
            {
                int len = str.Length;
                result += str.Substring(lastPlace, foundIndex-lastPlace) + newValue;
                lastPlace = foundIndex + searchValue.Length;
            }

            result += str.Substring(lastPlace);

            return result;
        }

        public static string PadLeft(string str, int totalWidth, char paddingChar)
        {
            if (str.Length >= totalWidth)
            {
                return str;
            }

            string pad = new string(paddingChar, totalWidth - str.Length);
            return pad + str;
        }

        public static string PadRight(string str, int totalWidth, char paddingChar)
        {
            if (str.Length >= totalWidth)
            {
                return str;
            }

            string pad = new string(paddingChar, totalWidth - str.Length);
            return str + pad;
        }

        public static string[] Split(string str, params string[] separators)
        {
            if (!Str.ContainsAny(str, separators))
            {
                return new string[] { str };
            }

            string recordSeparator = char.MaxValue.ToString();

            for (int i = 0; i < separators.Length; i++)
            {
                str = Str.Replace(str, separators[i], recordSeparator);
            }

            return str.Split(char.MaxValue);
        }

        public static bool StartsWith(string str, string value)
        {
            int index = str.IndexOf(value);
            return index == 0;
        }

        public static bool EndsWith(string str, string value)
        {
            int index = str.LastIndexOf(value);
            return index == str.Length - value.Length;
        }
    }
}
