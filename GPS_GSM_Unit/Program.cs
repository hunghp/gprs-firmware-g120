﻿using System;
using System.Text;
using System.Collections;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using Microsoft.SPOT.IO;
using System.IO.Ports;
using System.Threading;
using GHI.Pins;
using GHI.Utilities;
using GHI.Processor;

using GHI.IO;
using GHI.IO.Storage;

//Code for G120 chipset
namespace GPS_GSM_Unit
{            
    public class Program
    {
        //Hardware info
        static int _HW_VER = 16000; //go together with _HW_SERIAL
        static int _HW_SERIAL = 0;  //go together with _HW_VER
        static int _FW_VER = 0;                

        //Hardware variables
        static SerialPort _spGPS = new SerialPort("COM1", 4800, Parity.None, 8, StopBits.One);
        static SerialPort _spGSM = new SerialPort("COM2", 115200, Parity.None, 8, StopBits.One);
        static SerialPort _spCOM3 = new SerialPort("COM3", 19200, Parity.None, 8, StopBits.One);        

        static OutputPort _wakeGPSpin = new OutputPort(GHI.Pins.G120.P0_25, false);
        static OutputPort _wakeGSMpin = new OutputPort(GHI.Pins.G120.P3_26, false);
        static OutputPort _resetXBEE = new OutputPort(GHI.Pins.G120.P0_13, true);

        static OutputPort _tempPin_1 = new OutputPort(GHI.Pins.G120.P0_27, false);
        static OutputPort _tempPin_2 = new OutputPort(GHI.Pins.G120.P0_28, false); 
        static DS18B20 _ds18B20_1 = new DS18B20(_tempPin_1);
        static DS18B20 _ds18B20_2 = new DS18B20(_tempPin_2);

        static InputPort _onoff_1 = new InputPort(GHI.Pins.G120.P0_26, true, Port.ResistorMode.PullUp);       

        static AnalogInput _analog_0_5V_1 = new AnalogInput((Cpu.AnalogChannel)Cpu.AnalogChannel.ANALOG_5);  //P1.31   

        //Interrupt pin to send a complete package at anytime
        static InterruptPort _send_package_interrupt = new InterruptPort(GHI.Pins.G120.P2_2, true, Port.ResistorMode.PullUp, Port.InterruptMode.InterruptEdgeLow);
                      
        //LED////////////////////////
        //  _statusLED:
        //      1. Solid On, SD card mount failed.
        //      2. Blink Once after every 3 seconds and on going, reading setting text file from SD card failed.
        //      3. Keep Blinking non-stop, register wired and wireless sensor configuration failed.  Write sensors configuration to the database failed.
        //      4. Blink 3 times and stay off, load setting file and sensors from database OK.
        //
        // _gpsOnlineLED:
        //      1. Solid On, gps unit is power and receiving data.
        //      2. Solid On and blink 3 times every 3 seconds, gps unit is locked and have valid location data
        //
        // _gsmOnlineLED:
        //      1. Solid On, unit is powered and ready to send and receive.
        //      2. Blink 5 times, signal that an interrupt of requesting the system to transmit out a package
        //
        // _transmissionLED:
        //      1. Stay On while the system is transmitting or receiving data.
        static OutputPort _gpsOnlineLED = new OutputPort(GHI.Pins.G120.P0_1, false);
        static OutputPort _transmissionLED = new OutputPort(GHI.Pins.G120.P3_25, false);
        static OutputPort _gsmOnlineLED = new OutputPort(GHI.Pins.G120.P3_24, false);
        static OutputPort _statusLED = new OutputPort(GHI.Pins.G120.P0_0, false);       


        //System data objects and variables
        static GPSData _GPSDataObject = new GPSData(new DateTime(1980, 5, 13, 0, 0, 0), "", "", "", "", "");
        static DateTime _LastRTCUpdate = new DateTime(1980, 5, 13, 0, 0, 0);
        static bool _GSMUnitOnline = false;
        static bool _GPSUnitOnline = false;
        static bool _IsRTCSet = false;             
        static int _LogFileIndex = 0;
        static int _DownloadLogFileIndex = 0;
        static int MAX_LOG_FILE_SIZE = 800;  //in bytes
        static bool _GPSUnitLocked = false;       

        //System Settings variables
        static int _PROCESS_GPS_DATA_INTERVAL = 5;  //in seconds
        static int _TRANSMIT_INTERVAL = 5;  //default value 5 minutes
        static string _DEVICE_NAME = System.String.Empty;               
        static string _SIMPhoneNo = System.String.Empty;
        static string _ip = System.String.Empty;
        static string _port = System.String.Empty;        
        static string _apn_name = System.String.Empty;
        static string _apn_username = System.String.Empty;
        static string _apn_password = System.String.Empty;
        static int _wireless_sensor_offline_threshold = 10;  //Wireless Sensor offline in minute.  Default 10 minutes
        static string _epoch_time_stamp = System.String.Empty;
        static string _setting_id = System.String.Empty;
        static string _sensors_definition = System.String.Empty;        
        
        //Threads
        static Thread _gpsThread = new Thread(new ThreadStart(GPSThread));
        static Thread _doWork = new Thread(new ThreadStart(DoMainWork));
        static Thread _pointSixThread = new Thread(new ThreadStart(MonitorPointSixData));   //Thread that responsible for Point Six package download and parsing
       
        //XOR passphrase
        static byte[] _xorPass = new byte[] { 115, 99, 105, 101, 110, 116, 105, 115, 116, 115, 32, 105, 110, 118, 101, 115, 116, 105, 103, 97, 116, 101, 32, 116, 
                                      104, 97, 116, 32, 119, 104, 105, 99, 104, 32, 108, 114, 101, 97, 100, 121, 32, 105, 115, 59, 32, 101, 110, 103, 105, 
                                      110, 101, 101, 114, 115, 32, 99, 114, 101, 97, 116, 101, 32, 116, 104, 97, 116, 32, 119, 104, 105, 99, 104, 32, 104, 
                                      97, 115, 32, 110, 101, 118, 101, 114, 32, 98, 101, 101, 110, 32, 45, 32, 97, 108, 98, 101, 114, 116, 32, 101, 105, 110, 
                                      115, 116, 101, 105, 110 };        
        
       
        //Static sd card instance
        static SDCard _sdCard = new SDCard();
        static SDStorage _sdStor;        

        //Binary programming pin for xbee 900 module
        static OutputPort _xbee_cmd_pin = new OutputPort(GHI.Pins.G120.P1_17, false);


        /////////////////////////////////////////////////////
        //  Point Six Sensor Global variables              //
        /////////////////////////////////////////////////////
        static int _PointSixSensorBytesLength = 75;

        //Store newly received point six package
        static byte[] _PointSixPackage = new byte[75];
        static bool _NewPointSixPackageReceived = false;

        static DateTime _timeAdjustment;
        static DateTime _sensorDownloadDateTime;
        static string _REQ_DATA_CMD = "C33C0020";

        static string[] _byteStrArray = new string[75];
        static bool _stopWaiting = false;
        static byte[] _sendPacket = null;
        static int _ps_download_count_1 = 0;
        static int _ps_download_count_2 = 0;
        static int _oldestBlock = -1;               //-2: Need to get oldest block record but failed, -1: Doesn't need to get oldest block record, >-1: Block record acquired successfully.

        static ArrayList _PointSixSensorList = new ArrayList();
        static ArrayList _IncompleteDownloadList = new ArrayList();
        static SensorRecord _parsedSensorRecord = null;        


        public static void Main()
        {
            //RealTimeClock.SetDateTime(new DateTime(2016, 1, 29, 9, 40, 0));
            //Debug.Print("RTC Set.");
            //Debug.Print(RealTimeClock.GetDateTime().ToString());
            //Utility.SetLocalTime(RealTimeClock.GetDateTime());

            ////Detect system restart by WatchDog. Normally, you can read this flag ***ONLY ONCE*** on power up
            //if (GHI.Processor.Watchdog.LastResetCause == GHI.Processor.Watchdog.ResetCause.Watchdog)
            //{
            //    SpecialGlobalVar._SystemRebootByWatchDog = true;
            //}
            //else
            //{
            //    SpecialGlobalVar._SystemRebootNotByWatchDog = true;
            //}

            ////Enable WATCHDOG TIMER
            //GHI.Processor.Watchdog.Enable(32000);

            HUtility util = new HUtility();           

            //Mount the sd card and create an instance of it and assign to a static variable
            try
            {
                bool fs_ready = false;
                _sdCard.Mount();

                RemovableMedia.Insert += (a, b) =>
                {
                    fs_ready = true;
                };
               
                while (!fs_ready)
                {
                    Thread.Sleep(50);
                }

                _sdStor = new SDStorage(_sdCard);
            }
            catch (Exception ex)
            {
                Debug.Print("From [Main], mount sd card failed: " + ex.ToString());

                //System needs restart, set watchdog timer flag            
                SpecialGlobalVar._SystemNeedReboot = true;

                _statusLED.Write(true);

                //Stay here until system reboot by the Watch Dog
                while (true) 
                {
                    //Wait until system reset by watch dog
                }
            }


            //Load sensors from database
            DB db = new DB();
            if (db.OpenDB() == true)
            {
                _PointSixSensorList = db.GetSensorList();
                db.CloseDB();
            }     
            
            //Load settings from sd card           
            LoadSettingsFromSDCard();

            //Load incomplete download list if it exist
            LoadIncompleteDownloadList();

            //Indicate finish loading settings from SD Card and sensors from Database
            util.BlinkLED(_statusLED, 3, 300, LED_Blink_Type.ON_OFF);


            ///////////////test code/////////////////////////
            SensorRecord srTest = null;
            Debug.Print("---------------------------------REGISTERED SENSORS LIST---------------------------");
            foreach (var sensor in _PointSixSensorList)
            {
                srTest = sensor as SensorRecord;
                Debug.Print(srTest.ToString() + "\n");
            }

            Debug.Print("---------------------------------END LISTS-----------------------------------------");



            IncompleteDownloadRecord ir = null;
            Debug.Print("---------------------------------INCOMPLETE SENSORS LIST---------------------------");
            foreach (var record in _IncompleteDownloadList)
            {
                ir = record as IncompleteDownloadRecord;
                Debug.Print(ir.ToString() + "\n");
            }

            Debug.Print("---------------------------------END INCOMPLETE LIST-------------------------------");

            /////////////////////////////////////////////////



            //Initalize COM3 serial port for XBee 900 Transceiver
            //Open COM port
            if (util.InitializeComPort(_spCOM3) == true)
            {
                _spCOM3.DataReceived += _spCOM3_DataReceived;                

                //Change xbee destination address to default of 0505.            
                _xbee_cmd_pin.Write(true);           // Pull pin 16 high
                Thread.Sleep(100);
                string xbee_bin_cmd = "00050508";    //Set Destination Address to 0x0505
                byte[] xbee_bin_cmd_bytes = HTools.Hex.ToBytes(xbee_bin_cmd);
                _spCOM3.Write(xbee_bin_cmd_bytes, 0, xbee_bin_cmd_bytes.Length);
                Thread.Sleep(100);
                _xbee_cmd_pin.Write(false);          // Pull pin 16 low

                //And change xbee address mask to accept all incoming package from all sensor, change to 0x0000
                Thread.Sleep(1000);
                _xbee_cmd_pin.Write(true);      // Pull pin 16 high
                xbee_bin_cmd = "12000008";      //Set Address Mask to 0x0000
                xbee_bin_cmd_bytes = HTools.Hex.ToBytes(xbee_bin_cmd);
                _spCOM3.Write(xbee_bin_cmd_bytes, 0, xbee_bin_cmd_bytes.Length);
                Thread.Sleep(100);
                _xbee_cmd_pin.Write(false);     // Pull pin 16 low
            }
            else
            {
                //reset the system
                SpecialGlobalVar._SystemNeedReboot = true;
            }
            util = null; //set for GC to collect     
  
            //Set up interrupt pin            
            _send_package_interrupt.OnInterrupt += _send_package_interrupt_OnInterrupt;            
       
            //Start gps thread
            if (!_gpsThread.IsAlive)
                _gpsThread.Start();

            //Start monitoring thread
            if (!_pointSixThread.IsAlive)
                _pointSixThread.Start();

            //Initialize GSM
            SetGSMUnitBaudRate115200();
            InitializeGSMUnit();
                                      
            //Start main work Thread
            if (!_doWork.IsAlive)
                _doWork.Start();           
            
            Thread.Sleep(Timeout.Infinite);            
        }

        //Send out one complete package when interrupt occurred
        static void _send_package_interrupt_OnInterrupt(uint data1, uint data2, DateTime time)
        {            
            if (SpecialGlobalVar._TCPTransmissionInProcess == true)  //Transmission in process
            {
                _send_package_interrupt.ClearInterrupt();
                return;
            }
                        
            HUtility util = new HUtility();
            //Start transmitting one package
            SpecialGlobalVar._TCPTransmissionInProcess = true;

            //Indicate start processing data and preparing to transmit when interrupt happen
            util.BlinkLED(_gsmOnlineLED, 5, 100, LED_Blink_Type.OFF_ON);

            //Trasmit one complete data package through TCP/IP
            TransmitDeviceDataPackage();

            //Update registered sensors and their data to the database
            UpdateSensorRecordToDatabase();
                      
            //After finished transmitting, reset flag
            SpecialGlobalVar._TCPTransmissionInProcess = false;

            _send_package_interrupt.ClearInterrupt();            
        }

        //COM3 DataReceived event, listening for serial data from XBee 900 Transceiver
        static void _spCOM3_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            int readByte = 0;            

            while (_spCOM3.BytesToRead > 0)
            {
                //Look for the package header "C33C"
                if (SpecialGlobalVar._COM3TotalBytesRead == 0)
                {
                    readByte = _spCOM3.ReadByte();  //read first byte
                    if (readByte != 195)
                    {
                        continue;
                    }
                    else
                    {
                        SpecialGlobalVar._XBEE900SerialBuffer[0] = (byte)readByte;
                    }

                    readByte = _spCOM3.ReadByte(); //read second byte
                    if (readByte != 60)
                    {
                        continue;
                    }
                    else
                    {
                        SpecialGlobalVar._XBEE900SerialBuffer[1] = (byte)readByte;
                    }

                    readByte = _spCOM3.ReadByte(); //read third byte
                    if (readByte != 0)
                    {
                        continue;
                    }
                    else
                    {
                        SpecialGlobalVar._XBEE900SerialBuffer[2] = (byte)readByte;
                    }

                    SpecialGlobalVar._COM3TotalBytesRead = 3;
                }

                //package header found start reading byte to buffer if buffer is not full
                if (SpecialGlobalVar._COM3TotalBytesRead < _PointSixSensorBytesLength)
                {
                    SpecialGlobalVar._XBEE900SerialBuffer[SpecialGlobalVar._COM3TotalBytesRead] = (byte)_spCOM3.ReadByte();
                    SpecialGlobalVar._COM3TotalBytesRead++;
                }
                else //buffer is full, keep reading until no more bytes to read so the serial data event will fire again
                {
                    readByte = _spCOM3.ReadByte();
                }

            }// end while loop

            if (SpecialGlobalVar._COM3TotalBytesRead >= _PointSixSensorBytesLength)
            {
                SpecialGlobalVar._COM3TotalBytesRead = 0;  //reset variable
                _NewPointSixPackageReceived = true;
                
            }                        
        }


        #region [Threads Processes]       

        //GPS processing thread method
        static void GPSThread()
        {
            //Init GPS unit
            InitializeGPSUnit();           
                         
            while (true)
            {
                HUtility util = new HUtility();
                byte[] gpsByteArray;                                

                //Do this only if GPS is offline
                if (_GPSUnitOnline == false)
                {
                    _GPSUnitLocked = false;
                    InitializeGPSUnit();
                    continue;
                }

                gpsByteArray = util.SanitizeForUTF8(util.ReadUART(_spGPS));
                if (gpsByteArray == null)
                {
                    //GPS Unit could be off, try to initialize it again
                    _GPSUnitLocked = false;
                    InitializeGPSUnit();
                    continue;
                }
                
                if (gpsByteArray.Length > 0)
                {                    
                    _GPSUnitLocked = false;

                    string gpsMsg = new string(Encoding.UTF8.GetChars(gpsByteArray));

                    //process the message and parse out the gps data
                    string[] lines = gpsMsg.Split('\n');
                    for (int i = 0; i < lines.Length; i++)
                    {
                        lines[i] = lines[i].Trim();
                    }

                    for (int i = lines.Length - 1; i >= 0; i--)
                    {
                        //Check for minimum length
                        if (lines[i].Length < 10)
                            continue;
                        //Check for $GPRMC at the beginning of the line
                        if (String.Compare(lines[i].Substring(0, 6), "$GPRMC") != 0)
                            continue;

                        //Find checksum and verify the line
                        int asteriskIndex = lines[i].IndexOf("*");
                        if (asteriskIndex == -1)        //not found
                            continue;

                        //Check to see that there are only 2 more characters after the asterisk(checksum) and that 
                        //the sum of the asterisk index plus 2 (checksum string) is equal to the length of the entire line
                        string chksum = lines[i].Substring(asteriskIndex + 1).Trim();
                        if (chksum.Length != 2)
                            continue;
                        if (String.Compare(util.StringXOR(lines[i].Substring(1, lines[i].Length - 4)), chksum) != 0)
                            continue;

                        //line is valid, split line by comma
                        string[] gprmcData = lines[i].Split(',');

                        //Check to see if GPS unit is lock
                        if (String.Compare(gprmcData[2], "A") != 0)
                        {                                                                                
                            continue;
                        }

                        //GPS is locked now, assign gps data to GlobalVar.GPSDataObject
                        //Get the utc date
                        if (gprmcData[9].Length != 6)
                            continue;

                        if (gprmcData[1].Length != 10)
                            continue;

                        int year, month, day, hour, min, sec;
                        try
                        {
                            year = Convert.ToInt32(gprmcData[9].Substring(4)) + 2000;
                            month = Convert.ToInt32(gprmcData[9].Substring(2, 2));
                            day = Convert.ToInt32(gprmcData[9].Substring(0, 2));

                            hour = Convert.ToInt32(gprmcData[1].Substring(0, 2));
                            min = Convert.ToInt32(gprmcData[1].Substring(2, 2));
                            sec = Convert.ToInt32(gprmcData[1].Substring(4, 2));
                        }
                        catch
                        {
                            continue;
                        }

                        //Get UTC data time
                        DateTime utcDT = new DateTime(year, month, day, hour, min, sec, 0);                       
                        
                        //Check to see RTC is set, if not set it
                        if (_IsRTCSet == false)
                        {
                            try
                            {           
                                //Set the RTC
                                RTC rtc = new RTC();
                                rtc.SetRTC(utcDT);

                                Thread.Sleep(3000);

                                //Verify RTC is set
                                DateTime rtcDT = rtc.GetRTC();
                                int seconds_diff = (int)(rtcDT.Subtract(utcDT).Ticks / TimeSpan.TicksPerSecond);
                                if (seconds_diff >= 0 && seconds_diff <= 60)
                                {
                                    _IsRTCSet = true;

                                    //Set system local time                                
                                    Utility.SetLocalTime(rtcDT);
                                    _LastRTCUpdate = rtcDT;

                                    _sdStor.WriteToSettingFile("RTC_SET = 1", true);  
                                }
                            }
                            catch
                            {
                                //Exception happens, system need reboot/reset
                                SpecialGlobalVar._SystemNeedReboot = true;
                            }
                        }
                        else  //update RTC if it is a day old...
                        {
                            try
                            {
                                int hours_diff = (int)((utcDT.Subtract(_LastRTCUpdate).Ticks / TimeSpan.TicksPerSecond) / 3600);
                                if ( hours_diff >= 24)
                                {                                    
                                    RTC rtc = new RTC();
                                    rtc.SetRTC(utcDT);
                                    _IsRTCSet = true;

                                    //Set system local time                                
                                    Utility.SetLocalTime(rtc.GetRTC());
                                    rtc = null;
                                    _LastRTCUpdate = utcDT;
                                }                                      
                            }
                            catch
                            {
                                //Exception happens, system need reboot/reset
                                SpecialGlobalVar._SystemNeedReboot = true;
                            }
                        }


                        lock (_GPSDataObject)
                        {
                            _GPSDataObject = new GPSData(utcDT, gprmcData[5], gprmcData[6], gprmcData[3], gprmcData[4], gprmcData[7]);
                            _GPSUnitLocked = true;
                        }
                        
                        //Blink 3 times to indicate a valid locked postion data is acquired
                        util.BlinkLED(_gpsOnlineLED, 3, 300, LED_Blink_Type.OFF_ON);                       

                        break;
                    }
                }

                if (_GPSUnitLocked == false)
                {
                    if (_IsRTCSet == true)
                    {
                        RTC rtc = new RTC();                       
                        _GPSDataObject = new GPSData( rtc.GetRTC(), "", "", "", "", "");  //if it is 1970, then reading from RTC throw exception, battery might need to be change
                    }
                    else  //RTC not set, set GPSDataObject's date to my birthday
                    {
                        _GPSDataObject = new GPSData(new DateTime(1980, 5, 13, 0, 0, 0), "", "", "", "", "");
                    }
                }                

                ////Reset WatchDog Timer here
                //if (SpecialGlobalVar._SystemNeedReboot == false)
                //{
                //    //reset the watch dog, stop it from resetting the system.
                //    GHI.Processor.Watchdog.ResetCounter();
                //}
                
                Thread.Sleep(_PROCESS_GPS_DATA_INTERVAL * 1000);
            }                 
        }
        
        //Do main work thread
        static void DoMainWork()
        {
            RTC rtc = new RTC();
            HUtility util = new HUtility();
            DateTime rtcDT;                                                                   
            while (true)
            {
                //Save incomplete download list to file
                SaveIncompleteListToFile();

                //Check to see if GPS data is old, if so the GPS probably not locked or not online     
                rtcDT = rtc.GetRTC();     
                TimeSpan timeDiff = rtcDT.Subtract(_GPSDataObject.UTCDateTime);
                
                //bool isGPSDataOld = false;
                //if (util.GetTimeSpanSeconds(timeDiff) > 30)
                //{
                //    isGPSDataOld = true;
                //}

                if (SpecialGlobalVar._TCPTransmissionInProcess == false)  //Transmission not in process OK to start transmitting
                {
                    //Set flag
                    SpecialGlobalVar._TCPTransmissionInProcess = true;
                   
                    //Trasmit one complete data package through TCP/IP
                    TransmitDeviceDataPackage();                    

                    //After transmit at regular interval, update the database
                    UpdateSensorRecordToDatabase();

                    //Reset flag
                    SpecialGlobalVar._TCPTransmissionInProcess = false;                   
                }

                Thread.Sleep(_TRANSMIT_INTERVAL * 60000);
            }
        }

        //Monitor PointSix Wireless Sensors Process
        static void MonitorPointSixData()
        {
            string beaconPackage = "";
            while (true)
            {
                //No new package receive, continue
                if (!_NewPointSixPackageReceived)
                    continue;

                _NewPointSixPackageReceived = false;    //reset flag
                //SpecialGlobalVar._XBEE900SerialBuffer.CopyTo(_PointSixPackage, 0);    //copy to another arraylist

                //Check to see if it is a beacon package, ignore and continue
                if (SpecialGlobalVar._XBEE900SerialBuffer[2] != 0 && SpecialGlobalVar._XBEE900SerialBuffer[3] != 02)      //beacon package 0x0022
                {
                    //Clean out garbage in the serial port
                    _spCOM3.Flush();
                    _spCOM3.DiscardInBuffer();
                    _spCOM3.DiscardOutBuffer();
                    continue;
                }

                //Store the beacon package
                beaconPackage = HTools.ByteArr.ToHex(SpecialGlobalVar._XBEE900SerialBuffer);

                //Parse the package                
                _parsedSensorRecord = ParsePackage(SpecialGlobalVar._XBEE900SerialBuffer);

                //Unable to parse package, ignore and continue                
                if (_parsedSensorRecord == null)
                {
                    continue;
                }

                Debug.Print(_parsedSensorRecord.ToString());

                //Find the sensor, if it's not registered, continue                
                int sensorRecordIndex = -1;
                for (int i = 0; i < _PointSixSensorList.Count; i++)
                {
                    //Check to see if it's a wireless sensor object
                    if (_PointSixSensorList[i] is SensorRecord)
                    {
                        SensorRecord sr = _PointSixSensorList[i] as SensorRecord;
                        if (String.Compare(sr.SensorID, _parsedSensorRecord.SensorID) == 0)
                        {
                            sensorRecordIndex = i;
                            break;
                        }
                    }
                }

                if (sensorRecordIndex < 0)  //not found
                {
                    continue;
                }

                //Get the found sensor record from the registered list
                SensorRecord registeredSensor = _PointSixSensorList[sensorRecordIndex] as SensorRecord;

                //If it is a newly added sensor, no need to check to see if sensor is delay or not, no need to download anything, just update the record
                if (_parsedSensorRecord.SensorClock < 0)
                {
                    Send_PointSix_Sensor_ACK_CMD(_parsedSensorRecord.SensorID, 3);  //tell the sensor to sleep
                    UpdateSensorRecord(_parsedSensorRecord, registeredSensor);      //update sensor data               
                    continue;
                }


                DateTime startDT = DateTime.Now;
                DateTime endDT = DateTime.Now;
                int[] memoryBlockList = null;
                //Check to see if sensor is delay
                if (IsSensorDelay(registeredSensor, _parsedSensorRecord, _wireless_sensor_offline_threshold))
                {
                    startDT = registeredSensor.TimeStamp;
                    endDT = _parsedSensorRecord.TimeStamp;

                    //Get memory block list      
                    memoryBlockList = GetMemoryListForDownload(_parsedSensorRecord, registeredSensor, _oldestBlock);
                }
                else if (_IncompleteDownloadList.Count > 0)  //Check to see if sensor is on the incomplete download list
                {
                    bool found = false;
                    //Find the sensor in the list
                    for (int i = 0; i < _IncompleteDownloadList.Count; i++)
                    {
                        IncompleteDownloadRecord irec = _IncompleteDownloadList[i] as IncompleteDownloadRecord;
                        if (String.Compare(irec.SensorID, _parsedSensorRecord.SensorID) == 0)
                        {
                            startDT = irec.StartDate;
                            endDT = irec.EndDate;

                            //Memory rolled over, generate 2 sequences, from sr.BlockIndex to FF = 255 block, and from 0 to current block index
                            if (irec.StartMemBlock > irec.EndMemBlock)
                            {
                                ArrayList memArray1 = GetMemoryList(irec.StartMemBlock, 255);
                                ArrayList memArray2 = GetMemoryList(0, irec.EndMemBlock);
                                Array memList = Array.CreateInstance(typeof(int), memArray1.Count + memArray2.Count);
                                memArray1.CopyTo(memList, 0);
                                memArray2.CopyTo(memList, memArray1.Count);
                                memoryBlockList = (int[])memList;
                            }
                            else //Same block index or normal case            
                            {
                                ArrayList memArray = GetMemoryList(irec.StartMemBlock, irec.EndMemBlock);
                                Array memList = Array.CreateInstance(typeof(int), memArray.Count);
                                memArray.CopyTo(memList, 0);
                                memoryBlockList = (int[])memList;
                            }

                            //Delete this record from the list
                            _IncompleteDownloadList.RemoveAt(i);

                            found = true;
                            break;
                        }
                    }

                    if (found == false)
                        continue;
                }
                else
                {
                    Send_PointSix_Sensor_ACK_CMD(_parsedSensorRecord.SensorID, 3);  //tell the sensor to sleep
                    UpdateSensorRecord(_parsedSensorRecord, registeredSensor);      //update sensor data     
                    continue;
                }


                //Check to see if sensor clock of the current package is less than the recorded one,
                //we need to request the oldest log and download from there until recent received block                                
                if (_parsedSensorRecord.SensorClock < registeredSensor.SensorClock)
                {
                    //Need to get oldest record
                    _oldestBlock = GetOldestBlock(_parsedSensorRecord.SensorID.Trim());

                    //Get memory block list again if oldest block is required                
                    memoryBlockList = GetMemoryListForDownload(_parsedSensorRecord, registeredSensor, _oldestBlock);
                }

                /////////////////////////////////////////////////////////////
                //  Start downloading memory blocks from sensor procedure  //
                /////////////////////////////////////////////////////////////                           

                //Change xbee destination address to request log download for the sensor that just came in.
                _xbee_cmd_pin.Write(true);      // Pull pin 16 high
                Thread.Sleep(1);
                string xbee_bin_cmd = "00" + _parsedSensorRecord.SensorID.Substring(6, 2) + _parsedSensorRecord.SensorID.Substring(4, 2) + "08";    //Set Destination Address to the sensor address
                byte[] xbee_bin_cmd_bytes = HTools.Hex.ToBytes(xbee_bin_cmd);
                _spCOM3.Write(xbee_bin_cmd_bytes, 0, xbee_bin_cmd_bytes.Length);
                Thread.Sleep(1);

                //Set Address Mask to 0xFFFF, this will allow the sensor to communicate with the host without the host seeing packages from other sensors, 
                //this should eliminate interference and allow the host to collect all memory blocks in one session
                xbee_bin_cmd = "12FFFF08";
                xbee_bin_cmd_bytes = HTools.Hex.ToBytes(xbee_bin_cmd);
                _spCOM3.Write(xbee_bin_cmd_bytes, 0, xbee_bin_cmd_bytes.Length);
                Thread.Sleep(1);
                _xbee_cmd_pin.Write(false); // Pull pin 16 low               

                //Download history records from sensor
                DownloadHistoryRecordsAndSendItToHub(_parsedSensorRecord, memoryBlockList, startDT, endDT);

                //Update the sensor after trying to download to the most recent date time
                UpdateSensorRecord(_parsedSensorRecord, registeredSensor);

                //Write incomplete download list to file if there are any
                SaveIncompleteListToFile();

                //Update sensors to database
                UpdateSensorRecordToDatabase();

                
                //Set Address Mask back to 0x0000, this will allow all sensor to communicate with the host again, 
                _xbee_cmd_pin.Write(true);      // Pull pin 16 high
                Thread.Sleep(1);
                xbee_bin_cmd = "12000008";      // Set Address Mask to 0x0000
                xbee_bin_cmd_bytes = HTools.Hex.ToBytes(xbee_bin_cmd);
                _spCOM3.Write(xbee_bin_cmd_bytes, 0, xbee_bin_cmd_bytes.Length);
                Thread.Sleep(1);
                _xbee_cmd_pin.Write(false);     // Pull pin 16 low

            }

        }
       
        #endregion


        ////////////////////
        // Helper Methods //
        ////////////////////

        #region [Helper Methods]

        public static int DetectGSMSerialPortBaudRate()
        {
            int[] baud_array = new int[] { 115200, 9600, 19200, 38400, 4800 };
            HUtility util = new HUtility();
            
            for (int i = 0; i < baud_array.Length; i++)
            {
                //Close the GSM serial port first if it's open
                if (_spGSM.IsOpen)
                {
                    _spGSM.Close();
                }

                _spGSM = new SerialPort("COM2", baud_array[i], Parity.None, 8, StopBits.One);
                if (util.InitializeComPort(_spGSM) == true)
                {
                    if (util.IsGSMUnitAlive(_spGSM) == true)
                    {                        
                        return baud_array[i];
                    }
                    else
                    {
                        continue;
                    }
                }
                else
                {
                    return -1;
                }
            }
            return -2;            
        }

        //Set GSM Unit to the right baud rate of 115200
        public static void SetGSMUnitBaudRate115200()
        {
            HUtility util = new HUtility();
            int baud_rate = -1;                        
            while (true)
            {
                baud_rate = DetectGSMSerialPortBaudRate();                

                //If return baud rate is -1, this means the G120 module unable to initialize/open COM port
                //Need Watchdog to reset the system
                if (baud_rate == -1)
                {
                    SpecialGlobalVar._SystemNeedReboot = true;
                    break;
                }

                //If return baud rate is -2, this means the GSM module might not be on, wake it up 
                //then try to get it baud rate again
                if (baud_rate == -2)
                {
                    //Wake GSM unit   
                    
                    //test code
                    util.WriteToUART(_spGSM, "In Set Baudrate.", true);
                    ///

                    util.WakeGSM(_wakeGSMpin);
                    continue;
                }

                //If it's 115200, exit
                if (baud_rate == 115200)
                    break;

                //If the baudrate is something else, set it the baudrate to 115200   
                //close the port first
                if (_spGSM.IsOpen)
                {
                    _spGSM.Close();
                }                
                _spGSM = new SerialPort("COM2", baud_rate, Parity.None, 8, StopBits.One);
                if (util.InitializeComPort(_spGSM) == true)
                {
                    //Set the baud rate and let the loop check it.                    
                    util.WriteToUART(_spGSM, "AT+IPR=115200&W", true);
                    Thread.Sleep(1000);
                    continue;
                                     
                }
                else
                { 
                    //big problem G120 can't initialize com port, system needs reset
                    SpecialGlobalVar._SystemNeedReboot = true;
                    break;
                }                    
            }                                                             
        }
    
        
        //Initializing GSM unit
        public static void InitializeGSMUnit()
        {
            HUtility util = new HUtility();           

            //Loop until GSM unit is intialized
            while (true)
            {                                             
                if (util.InitializeComPort(_spGSM) == true)
                {                                          
                    //Check to see if the unit is still alive
                    if (util.IsGSMUnitAlive(_spGSM) == false || SpecialGlobalVar._TCPConnectionRetries > 3)
                    {
                        _gsmOnlineLED.Write(false);
                        _GSMUnitOnline = false;

                        while (util.IsGSMUnitAlive(_spGSM) == false)
                        {                            
                            util.WakeGSM(_wakeGSMpin);
                        }

                        //Reset variable "SpecialGlobalVar._TCPConnectionRetries"
                        SpecialGlobalVar._TCPConnectionRetries = 0;
                        SpecialGlobalVar._GSMUnitNormalPowerDown = true;
                    }
                                                           
                    _gsmOnlineLED.Write(true);
                    _GSMUnitOnline = true;                                                           

                    break;
                }
                else
                {
                    _GSMUnitOnline = false;
                    _gsmOnlineLED.Write(false);
                    continue;
                }
            }

            //Reset flags
            SpecialGlobalVar._GSMUnitNormalPowerDown = false;
            SpecialGlobalVar._SystemRebootByWatchDog = false;
            SpecialGlobalVar._SystemRebootNotByWatchDog = false;

            //Set phone to full functionality
            if (util.SetPhoneFullFunctionality(_spGSM) == true)
            {
                util.BlinkLED(_gsmOnlineLED, 5, 200, LED_Blink_Type.OFF_ON);
            }

            //Is SIM ready
            if (util.IsSIMReady(_spGSM) == true)
            {
                util.BlinkLED(_gsmOnlineLED, 5, 200, LED_Blink_Type.OFF_ON);
            }

            if (util.SetAPNConfiguration(_spGSM, _apn_name, _apn_username, _apn_password) == true)
            {
                util.BlinkLED(_gsmOnlineLED, 5, 200, LED_Blink_Type.OFF_ON);
            }

            if (util.RegisteredAPN(_spGSM) == true)
            {
                util.BlinkLED(_gsmOnlineLED, 5, 200, LED_Blink_Type.OFF_ON);
            }

            if (util.SetSingleTCPSession(_spGSM) == true)
            {
                util.BlinkLED(_gsmOnlineLED, 5, 200, LED_Blink_Type.OFF_ON);
            }

            //Get subscriber phone number
            _SIMPhoneNo = util.GetSubscriberPhoneNumber(_spGSM);


            //if (SpecialGlobalVar._GSMUnitNormalPowerDown == true || SpecialGlobalVar._SystemRebootByWatchDog == true || SpecialGlobalVar._SystemRebootNotByWatchDog == true)
            //{
                
            //}

            util = null;
        }

        //Initialize GPS unit
        public static bool InitializeGPSUnit()
        {
            HUtility util = new HUtility();
            //Loop until GPS unit is initialized
            while (true)
            {
                if (util.InitializeComPort(_spGPS) == true)
                {
                    while (util.IsGPSUnitOn(_spGPS) == false)
                    {
                        _gpsOnlineLED.Write(false);
                        _GPSUnitOnline = false;                                                
                        util.WakeGPS(_wakeGPSpin);
                    }
                    util.SendGPSSetupCmd(_spGPS);

                    _GPSUnitOnline = true;
                    _gpsOnlineLED.Write(true);
                    break;
                }
                else
                {
                    _gpsOnlineLED.Write(false);
                    _GPSUnitOnline = false;                                        
                    continue;
                }
            }//End GPS initialize loop
            util = null;
            return true;
        }

        public static IOData GetIOReadings()
        {
            double temp1 = 0, temp2 = 0, analog1 = 0;
            int on_off_1 = 0;
            _ds18B20_1 = new DS18B20(_tempPin_1);
            _ds18B20_2 = new DS18B20(_tempPin_2);
            //Get temperature IO first
            temp1 = _ds18B20_1.GetTempInCelsius();
            temp2 = _ds18B20_2.GetTempInCelsius();

            //Get on off reading IO
            if (_onoff_1.Read() == true)
                on_off_1 = 0;
            else
                on_off_1 = 1;
           
            //Get 0-5V analog reading            
            analog1 = _analog_0_5V_1.Read();
            analog1 = analog1 * 3.3;
            analog1 = analog1 / 0.66;           

            return new IOData(temp1.ToString(), temp2.ToString(), on_off_1.ToString(), analog1.ToString());
        }

        public static byte[] PadLeft(byte[] input, int totalLength)
        {
            if (input.Length >= totalLength)
            {
                return input;
            }

            byte[] padded = new byte[totalLength];
            int padStart = totalLength - input.Length;

            for (int i = 0; i < totalLength; i++)
            {
                if (i < padStart)
                {
                    padded[i] = 0;
                }
                else
                {
                    padded[i] = input[i - padStart];
                }
            }
            return padded;
        }

        public static byte[] PadRight(byte[] input, int totalLength)
        {
            if (input.Length >= totalLength)
            {
                return input;
            }
            byte[] result = new byte[totalLength];

            for (int i=0; i<totalLength; i++)
            {
                if (i < input.Length - 1)
                {
                    result[i] = input[i];
                }
                else
                {
                    return result;
                }
            }
            return result;
        }

        public static uint Do2complement(int value)
        {
            //make it positive first
            value = value * -1;

            //assign to unsigned integer
            uint new_value = (uint)value;

            new_value = ~new_value + 1;
            return new_value;
        }
       
        public static byte[] CreateBytesPackageToSend(IOData iodata)
        {
            HUtility util = new HUtility();                      
            ArrayList data_package_byte_array = new ArrayList();            
                    
            //data package start
            //Package type byte, 0xDD - regular data package
            byte[] package_type = BitConverter.GetBytes(221);
            data_package_byte_array.Add(new MyByte(package_type[0]));   

            //Production year and hardware version, hardware version contain production year        
            byte[] serial_header_b_array = BitConverter.GetBytes(_HW_VER);            
            data_package_byte_array.Add(new MyByte(serial_header_b_array[0]));
            data_package_byte_array.Add(new MyByte(serial_header_b_array[1]));
           
            //Hardware Serial counter
            byte[] serial_counter_b_array = PadRight(BitConverter.GetBytes(_HW_SERIAL), 2);        
            data_package_byte_array.Add(new MyByte(serial_counter_b_array[0]));
            data_package_byte_array.Add(new MyByte(serial_counter_b_array[1]));

            //Firmware version
            byte[] firmware_ver = BitConverter.GetBytes(_FW_VER);
            data_package_byte_array.Add(new MyByte(firmware_ver[0]));            
            
            //Sensors data bytes length
            data_package_byte_array.Add(new MyByte(0x00));
            data_package_byte_array.Add(new MyByte(0x00));
           
            //Setting ID
            byte[] settingID = PadRight(BitConverter.GetBytes(int.Parse(_setting_id)), 4);
            data_package_byte_array.Add(new MyByte(settingID[0]));
            data_package_byte_array.Add(new MyByte(settingID[1]));
            data_package_byte_array.Add(new MyByte(settingID[2]));
            data_package_byte_array.Add(new MyByte(settingID[3]));

            //epoch date time, what datetime we need to put here?  Normally if gps is locked and updating, using _GPSDataObject is fine.
            //What about when gps is not locked or updating? We must check to see if RTC is set, if set, then use RTC or system datetime.
            //If RTC is not set, we must include a default time for HUB to know that datetime from the unit is invalid, HUB then can use 
            //HUB's system datetime.
            long epochTime = 0;
            if (_GPSUnitLocked == true)
                epochTime = ToEpochTicks(_GPSDataObject.UTCDateTime);                
            else if (_GPSUnitLocked == false && _IsRTCSet == true)
                epochTime = ToEpochTicks(DateTime.Now);
            else
                epochTime = ToEpochTicks(new DateTime(1980, 5, 13, 0, 0, 0));
                                    
            byte[] epochTime_b_array = PadRight(BitConverter.GetBytes((uint)epochTime), 4);
            data_package_byte_array.Add(new MyByte(epochTime_b_array[0]));
            data_package_byte_array.Add(new MyByte(epochTime_b_array[1]));
            data_package_byte_array.Add(new MyByte(epochTime_b_array[2]));
            data_package_byte_array.Add(new MyByte(epochTime_b_array[3]));            

            //longitude and latitude
            double  dlatDecimal = Convert.ToDouble(util.GetGPRMCDecimal(_GPSDataObject.Latitude + _GPSDataObject.LatitudeNS, true)) * 1000000;
            int latitude = (int)dlatDecimal;
            double dlongDecimal = Convert.ToDouble(util.GetGPRMCDecimal(_GPSDataObject.Longitude + _GPSDataObject.LongitudeEW, false)) * 1000000;
            int longitude = (int)dlongDecimal;

            //convert to 2's complement if the number is negative
            uint u_latitude = 0, u_longitude = 0;
            if (latitude < 0)
            {
                u_latitude = Do2complement(latitude);
            }
            else
            {
                u_latitude = (uint)latitude;
            }

            if (longitude < 0)
            {
                u_longitude = Do2complement(longitude);
            }
            else
            {
                u_longitude = (uint)longitude;
            }

            byte[] latitude_b_array = PadRight(BitConverter.GetBytes(u_latitude), 4);
            data_package_byte_array.Add(new MyByte(latitude_b_array[0]));
            data_package_byte_array.Add(new MyByte(latitude_b_array[1]));
            data_package_byte_array.Add(new MyByte(latitude_b_array[2]));
            data_package_byte_array.Add(new MyByte(latitude_b_array[3]));

            byte[] longitude_b_array = PadRight(BitConverter.GetBytes(u_longitude), 4);
            data_package_byte_array.Add(new MyByte(longitude_b_array[0]));
            data_package_byte_array.Add(new MyByte(longitude_b_array[1]));
            data_package_byte_array.Add(new MyByte(longitude_b_array[2]));
            data_package_byte_array.Add(new MyByte(longitude_b_array[3]));           
            
            //Start looping through _registeredSensor to see what wired and wireless sensor are required
            //Get the current datetime from RTC            
            DateTime currentDT = DateTime.Now;           
            foreach (var sensorObject in _PointSixSensorList)
            {
                //If sensor is point six wireless sensor
                if (sensorObject is SensorRecord)
                {
                    SensorRecord sensor = sensorObject as SensorRecord;

                    //Check for which io are required
                    if (sensor.IOFlag == 1)  //io1 only
                    {                        
                        byte[] io1_b_array = PadRight(BitConverter.GetBytes(sensor.RawIOValue1), 2);
                        data_package_byte_array.Add(new MyByte(io1_b_array[0]));
                        data_package_byte_array.Add(new MyByte(io1_b_array[1]));
                    }
                    else if (sensor.IOFlag == 2) //io2 only
                    {
                        byte[] io2_b_array = PadRight(BitConverter.GetBytes(sensor.RawIOValue2), 2);
                        data_package_byte_array.Add(new MyByte(io2_b_array[0]));
                        data_package_byte_array.Add(new MyByte(io2_b_array[1]));
                    }
                    else //both io1 and io2 are required
                    {
                        //raw reading
                        byte[] ios_b_array = PadRight(BitConverter.GetBytes(sensor.RawIOValue1), 2);
                        data_package_byte_array.Add(new MyByte(ios_b_array[0]));
                        data_package_byte_array.Add(new MyByte(ios_b_array[1]));

                        ios_b_array = PadRight(BitConverter.GetBytes(sensor.RawIOValue2), 2);
                        data_package_byte_array.Add(new MyByte(ios_b_array[0]));
                        data_package_byte_array.Add(new MyByte(ios_b_array[1]));
                    }

                    //battery reading, this can be 0-100, 101 => battery was not reset from last battery change or need to change battery immediately
                    //102 => the sensor does not support battery reading, 103 => sensor is offline, no IOs readings or battery reading
                    
                    //Check for sensor offline first
                    int timeDiffInMinutes = 0;
                    timeDiffInMinutes = currentDT.Subtract(sensor.TimeStamp).Minutes;
                    byte[] battery_b_array;
                    if (timeDiffInMinutes > _wireless_sensor_offline_threshold)
                    {
                        battery_b_array = BitConverter.GetBytes(103);
                        data_package_byte_array.Add(new MyByte(battery_b_array[0]));
                    }
                    else
                    {
                        battery_b_array = BitConverter.GetBytes(sensor.Battery);
                        data_package_byte_array.Add(new MyByte(battery_b_array[0]));
                    }                    
                }

                //If sensor is wired grps port
                if (sensorObject is GPRSPort)
                {
                    //Check to see which port is required
                    GPRSPort sensor = sensorObject as GPRSPort;
                    if (String.Compare(sensor.PortName, "io1") == 0)  
                    {
                        double d_temp1 = Convert.ToDouble(iodata.Temp1) * 100;
                        int temp1 = (int)d_temp1;

                        //convert to 2's complement if the number is negative
                        uint u_temp1 = 0; ushort utemp1 = 0;

                        if (temp1 < 0)
                        {
                            u_temp1 = Do2complement(temp1);
                            utemp1 = (ushort)u_temp1;
                        }
                        else
                        {
                            utemp1 = (ushort)temp1;
                        }

                        byte[] temp1_b_array = PadRight(BitConverter.GetBytes(utemp1), 2);
                        data_package_byte_array.Add(new MyByte(temp1_b_array[0]));
                        data_package_byte_array.Add(new MyByte(temp1_b_array[1]));                        
                    }
                    else if (String.Compare(sensor.PortName, "io2") == 0)
                    {
                        double d_temp2 = Convert.ToDouble(iodata.Temp2) * 100;
                        int temp2 = (int)d_temp2;

                        //convert to 2's complement if the number is negative
                        uint u_temp2 = 0; ushort utemp2 = 0;

                        if (temp2 < 0)
                        {
                            u_temp2 = Do2complement(temp2);
                            utemp2 = (ushort)u_temp2;
                        }
                        else
                        {
                            utemp2 = (ushort)temp2;
                        }

                        byte[] temp2_b_array = PadRight(BitConverter.GetBytes(utemp2), 2);
                        data_package_byte_array.Add(new MyByte(temp2_b_array[0]));
                        data_package_byte_array.Add(new MyByte(temp2_b_array[1]));                        
                    }
                    else if (String.Compare(sensor.PortName, "io3") == 0)
                    {
                        //On, Off byte
                        int onoff1 = Convert.ToInt32(iodata.OnOff1);

                        if (onoff1 == 1)
                            data_package_byte_array.Add(new MyByte(0x01));
                        else
                            data_package_byte_array.Add(new MyByte(0x00));                       
                    }
                    else if (string.Compare(sensor.PortName, "io4") == 0)
                    {
                        //Voltage 0-5V
                        double d_voltage1 = Convert.ToDouble(iodata.Analog1) * 100;
                        int voltage1 = (int)d_voltage1;

                        byte[] volt1_b_array = PadRight(BitConverter.GetBytes(voltage1), 2);
                        data_package_byte_array.Add(new MyByte(volt1_b_array[0]));
                        data_package_byte_array.Add(new MyByte(volt1_b_array[1]));                                              
                    }
                }
            }//end for
            

            byte[] full_package = new byte[data_package_byte_array.Count + 6];   //2bytes Header, 2bytes CRC16, 2bytes Footer
            byte[] data_package_without_headers_and_crc = new byte[data_package_byte_array.Count];
            
            MyByte myByte;
            for (int i = 0; i < data_package_byte_array.Count; i++)
            {
                myByte = data_package_byte_array[i] as MyByte;
                data_package_without_headers_and_crc[i] = myByte.TheByte;
            }

           // Debug.Print("Raw, no header or crc: " + HTools.ByteArr.ToHex(data_package_without_headers_and_crc));

            //package length
            byte[] sensorDataPackageLengthByteArray = PadRight(BitConverter.GetBytes(full_package.Length), 2);
            data_package_without_headers_and_crc[6] = sensorDataPackageLengthByteArray[0];
            data_package_without_headers_and_crc[7] = sensorDataPackageLengthByteArray[1];

            //calculate xor and crc
            //get xor first                                   
            byte[] xor_data_package = util.PassPharseXOR(data_package_without_headers_and_crc, _xorPass);            
            //get crc
            byte[] crc = Crc16.ComputeBytes(xor_data_package);


            //add header "HD"
            full_package[0] = 0x48;
            full_package[1] = 0x44;

            //Add xor_data_package to full_package
            Array.Copy(xor_data_package, 0, full_package, 2, xor_data_package.Length);

            //add crc
            full_package[xor_data_package.Length + 2] = crc[0];
            full_package[xor_data_package.Length + 3] = crc[1];

            //end package
            full_package[xor_data_package.Length + 4] = 0x44;
            full_package[xor_data_package.Length + 5] = 0x48;

            util = null;

            Debug.Print("Full XOR and CRC: " + HTools.ByteArr.ToHex(full_package));
            
            return full_package;            
        }

        public static Queue CreateDownloadLogBytePackagesForPointSixSensor(Queue sensorRecordQueue, int maxNumberOfSensorRecord)
        {
            if (sensorRecordQueue == null) //null variable, return null
                return null;

            if (sensorRecordQueue.Count <= 0)  //empty list, just return null.
                return null;

            HUtility util = new HUtility();
            Queue downloadLogStringByteQueue = new Queue();

            //Keep going until no more sensor record in the array list
            while (sensorRecordQueue.Count > 0)
            {                
                ArrayList data_package_byte_array = new ArrayList();

                //data package start
                //Package type byte, 0xD0 - downloaded log data package
                byte[] package_type = BitConverter.GetBytes(208);
                data_package_byte_array.Add(new MyByte(package_type[0]));

                //Production year and hardware version, hardware version contain production year        
                byte[] serial_header_b_array = BitConverter.GetBytes(_HW_VER);
                data_package_byte_array.Add(new MyByte(serial_header_b_array[0]));
                data_package_byte_array.Add(new MyByte(serial_header_b_array[1]));

                //Hardware Serial counter
                byte[] serial_counter_b_array = PadRight(BitConverter.GetBytes(_HW_SERIAL), 2);
                data_package_byte_array.Add(new MyByte(serial_counter_b_array[0]));
                data_package_byte_array.Add(new MyByte(serial_counter_b_array[1]));

                //Firmware version
                byte[] firmware_ver = BitConverter.GetBytes(_FW_VER);
                data_package_byte_array.Add(new MyByte(firmware_ver[0]));

                //Sensors data bytes length
                data_package_byte_array.Add(new MyByte(0x00));
                data_package_byte_array.Add(new MyByte(0x00));

                //Setting ID
                byte[] settingID = PadRight(BitConverter.GetBytes(int.Parse(_setting_id)), 4);
                data_package_byte_array.Add(new MyByte(settingID[0]));
                data_package_byte_array.Add(new MyByte(settingID[1]));
                data_package_byte_array.Add(new MyByte(settingID[2]));
                data_package_byte_array.Add(new MyByte(settingID[3]));

                //Sensor Serial Number 
                object obj = sensorRecordQueue.Peek();
                SensorRecord sr = obj as SensorRecord;             
                UInt64 sensor_serial_in_dec = util.ConvertToUInt64(sr.SensorID);
                byte[] sensor_serial = PadRight(BitConverter.GetBytes(sensor_serial_in_dec), 8);
                data_package_byte_array.Add(new MyByte(sensor_serial[0]));
                data_package_byte_array.Add(new MyByte(sensor_serial[1]));
                data_package_byte_array.Add(new MyByte(sensor_serial[2]));
                data_package_byte_array.Add(new MyByte(sensor_serial[3]));
                data_package_byte_array.Add(new MyByte(sensor_serial[4]));
                data_package_byte_array.Add(new MyByte(sensor_serial[5]));
                data_package_byte_array.Add(new MyByte(sensor_serial[6]));
                data_package_byte_array.Add(new MyByte(sensor_serial[7]));


                //Create sensor reading and add to the package one by one                
                long epochTime = 0;
                int for_loop_count = 0;
                //Determine how many sensor records left
                int remainingSensorRecord = sensorRecordQueue.Count;
                if (remainingSensorRecord >= maxNumberOfSensorRecord)
                    for_loop_count = maxNumberOfSensorRecord;
                else
                    for_loop_count = remainingSensorRecord;

                for (int i = 0; i < for_loop_count; i++)                                                    
                {                    
                    sr = sensorRecordQueue.Dequeue() as SensorRecord;

                    //Get date time  
                    epochTime = ToEpochTicks(sr.TimeStamp);                    
                    byte[] epochTime_b_array = PadRight(BitConverter.GetBytes((uint)epochTime), 4);
                    data_package_byte_array.Add(new MyByte(epochTime_b_array[0]));
                    data_package_byte_array.Add(new MyByte(epochTime_b_array[1]));
                    data_package_byte_array.Add(new MyByte(epochTime_b_array[2]));
                    data_package_byte_array.Add(new MyByte(epochTime_b_array[3]));

                    //Get IOs depend on the setting
                    //Check for which io are required
                    if (sr.IOFlag == 1)  //io1 only
                    {
                        byte[] io1_b_array = PadRight(BitConverter.GetBytes(sr.RawIOValue1), 2);
                        data_package_byte_array.Add(new MyByte(io1_b_array[0]));
                        data_package_byte_array.Add(new MyByte(io1_b_array[1]));
                    }
                    else if (sr.IOFlag == 2) //io2 only
                    {
                        byte[] io2_b_array = PadRight(BitConverter.GetBytes(sr.RawIOValue2), 2);
                        data_package_byte_array.Add(new MyByte(io2_b_array[0]));
                        data_package_byte_array.Add(new MyByte(io2_b_array[1]));
                    }
                    else //both io1 and io2 are required
                    {
                        byte[] ios_b_array = PadRight(BitConverter.GetBytes(sr.RawIOValue1), 2);
                        data_package_byte_array.Add(new MyByte(ios_b_array[0]));
                        data_package_byte_array.Add(new MyByte(ios_b_array[1]));

                        ios_b_array = PadRight(BitConverter.GetBytes(sr.RawIOValue2), 2);
                        data_package_byte_array.Add(new MyByte(ios_b_array[0]));
                        data_package_byte_array.Add(new MyByte(ios_b_array[1]));
                    }

                    //Add battery reading
                    byte[] battery_b_array = BitConverter.GetBytes(sr.Battery);
                    data_package_byte_array.Add(new MyByte(battery_b_array[0]));

                }

                byte[] full_package = new byte[data_package_byte_array.Count + 6];   //2bytes Header, 2bytes CRC16, 2bytes Footer
                byte[] data_package_without_headers_and_crc = new byte[data_package_byte_array.Count];

                MyByte myByte;
                for (int i = 0; i < data_package_byte_array.Count; i++)
                {
                    myByte = data_package_byte_array[i] as MyByte;
                    data_package_without_headers_and_crc[i] = myByte.TheByte;
                }

                // Debug.Print("Raw, no header or crc: " + HTools.ByteArr.ToHex(data_package_without_headers_and_crc));

                //package length
                byte[] sensorDataPackageLengthByteArray = PadRight(BitConverter.GetBytes(full_package.Length), 2);
                data_package_without_headers_and_crc[6] = sensorDataPackageLengthByteArray[0];
                data_package_without_headers_and_crc[7] = sensorDataPackageLengthByteArray[1];

                //calculate xor and crc
                //get xor first                                   
                byte[] xor_data_package = util.PassPharseXOR(data_package_without_headers_and_crc, _xorPass);
                //get crc
                byte[] crc = Crc16.ComputeBytes(xor_data_package);

                //add header "HD"
                full_package[0] = 0x48;
                full_package[1] = 0x44;

                //Add xor_data_package to full_package
                Array.Copy(xor_data_package, 0, full_package, 2, xor_data_package.Length);

                //add crc
                full_package[xor_data_package.Length + 2] = crc[0];
                full_package[xor_data_package.Length + 3] = crc[1];

                //end package
                full_package[xor_data_package.Length + 4] = 0x44;
                full_package[xor_data_package.Length + 5] = 0x48;

                //Add current package to ArrayList
                downloadLogStringByteQueue.Enqueue(util.ByteArrayToHexString(full_package));

            }//end while loop

            util = null;

            // Debug.Print("Full XOR and CRC: " + HTools.ByteArr.ToHex(full_package));

            return downloadLogStringByteQueue;                                       
        }

        public static void UploadHistoryLog(LOG_FILE_TYPE logFileType)
        {
            //Try to see if any data log file exist if so read and send all the recorded data package     
            HUB_RESPONSE hub_response = new HUB_RESPONSE(HUB_RESPONSE_CODE.SEND_FAILED, null);            
                                                
            while (true)
            {             
                ArrayList dataLogList = _sdStor.ReadDataLogFile(logFileType);                
                if (dataLogList == null)
                {
                    break;
                }

                if (dataLogList.Count <= 0)
                {
                    break;
                }

                //Start sending log data, 
                int MAX_SEND_DATA_PACKAGE = 5;

                if (logFileType == LOG_FILE_TYPE.REGULAR_LOG_FILE)
                    MAX_SEND_DATA_PACKAGE = 5;
                else
                    MAX_SEND_DATA_PACKAGE = 1;

                string history_data = null;
                ArrayList logs_to_send_list = new ArrayList();         
                while (dataLogList.Count > 0)
                {
                    //take out data package to send, up to MAX_SEND_DATA_PACKAGE size
                    int counter = 0;
                    foreach (var log in dataLogList)
                    {
                        if (counter >= MAX_SEND_DATA_PACKAGE)
                        {
                            counter = 0;  //reset counter
                            break;
                        }

                        logs_to_send_list.Add(log);  //add to send list
                        counter++;
                    }


                    //turn send list to one string
                    foreach (var log in logs_to_send_list)
                    {
                        if (log == null)
                            continue;
                        if (String.Compare(log.ToString(), "") == 0)
                            continue;

                        history_data += log;
                    }

                    //if history_data is empty continue
                    if (String.Compare(history_data, "") == 0 || history_data == null)
                    {
                        foreach (var log in logs_to_send_list)
                        {
                            dataLogList.Remove(log);
                        }
                        continue;
                    }


                    //Send history log data package to server
                    HUtility util = new HUtility();
                    byte[] byte_dataPackage = util.HexStringToByteArray(history_data);
                    hub_response = util.SendMsgOverTCPConnectionToHub(_spGSM, byte_dataPackage, 8000);

                    if (hub_response.RESPONSE_CODE == HUB_RESPONSE_CODE.SEND_FAILED)     //send history data log but send failed or no response from HUB
                    {
                        //put back to the log into Data text file
                        if (logFileType == LOG_FILE_TYPE.REGULAR_LOG_FILE)
                        {
                            _sdStor.WriteUndeliverableDataToDataLogFile(dataLogList, _LogFileIndex, out _LogFileIndex, MAX_LOG_FILE_SIZE, logFileType);
                            break;
                        }
                        else
                        {
                            _sdStor.WriteUndeliverableDataToDataLogFile(dataLogList, _DownloadLogFileIndex, out _DownloadLogFileIndex, MAX_LOG_FILE_SIZE, logFileType);
                            break;
                        }                        
                    }
                    else if (hub_response.RESPONSE_CODE == HUB_RESPONSE_CODE.OK_WITH_REQUEST_SETTING)
                    {
                        HandleHubRequestForSetting();
                    }
                    else if (hub_response.RESPONSE_CODE == HUB_RESPONSE_CODE.OK_WITH_SET_SETTING)
                    {
                        HandleHubCommandForSetSetting(hub_response.ResponseByteArray);
                    }
                    else  //send history data log OK or Hub response NOT OK.  If NOT OK (NOK) received from Hub, still remove from  list because we don't want to store invalid packages.                                   
                    {
                        //remove send history logs from dataLogList
                        foreach (var log in logs_to_send_list)
                        {
                            dataLogList.Remove(log);
                        }

                        //reset variables                                            
                        history_data = "";
                        logs_to_send_list.Clear();
                        continue;
                    }

                }//end while loop                                

                //if send failed or no response from HUB, HUB might be slow or down or in some kind fo trouble, just stop uploading
                if (hub_response.RESPONSE_CODE == HUB_RESPONSE_CODE.SEND_FAILED)
                {
                    break;
                }

            }//end while loop   

            Debug.GC(true);
        }

        //padleft for string
        public static string StringPadleft(string str, char padChar, int total_padded_length)
        {
            if (str.Length >= total_padded_length)
                return str;


            int length = total_padded_length - str.Length;
            string newStr = "";
            for (int i = 0; i < length; i++)
            {
                newStr = newStr + padChar;
            }
            newStr = newStr + str;
            return newStr;
        }
        
        //Handle Hub request for setting, handle command REQ_SETTING
        public static void HandleHubRequestForSetting()
        {
            HUtility util  = new HUtility();
            HUB_RESPONSE response = new HUB_RESPONSE(HUB_RESPONSE_CODE.SEND_FAILED, null);            
            int MAX_RETRIES = 0;
            int retry_count = 0;
            while(true)
            {                
                //Create setting package
                string serial = _HW_VER.ToString() + StringPadleft(_HW_SERIAL.ToString(), '0', 5);
                string fw_ver = _FW_VER.ToString();
                string name = _DEVICE_NAME;
                string transmit_int = _TRANSMIT_INTERVAL.ToString();  //convert to minutes
                string ip = _ip;
                string port = _port;
                string sim_number = _SIMPhoneNo;
                string apn_name = _apn_name;
                string apn_username = _apn_username;
                string apn_password = _apn_password;
                string offline_threshold = _wireless_sensor_offline_threshold.ToString();
                string setting_id = _setting_id;
                string sensors_definition = _sensors_definition;
                StringBuilder sdsb = new StringBuilder(_sensors_definition);
                sdsb.Replace("DEVICE_SERIAL", serial);

                string setting = serial + "|" + fw_ver + "|" + name + "|" + transmit_int + "|" + offline_threshold + "|" + ip + "|" + port + "|" + sim_number + "|" + _apn_name + "|" + apn_username + "|" +
                                 apn_password + "|" + _epoch_time_stamp + "|" + _setting_id + "|" + sdsb.ToString().Trim() + "|";

                //CRC computation
                byte[] crc = Crc16.ComputeBytes(Encoding.UTF8.GetBytes(setting));
                byte[] reverse_crc = new byte[2];
                reverse_crc[0] = crc[1];
                reverse_crc[1] = crc[0];
                string crcStr = HTools.ByteArr.ToHex(reverse_crc);
                string settingStr = HTools.ByteArr.ToHex(Encoding.UTF8.GetBytes(setting));
                //Debug.Print(settingStr);

                //Xor with passphrase
                byte[] xor_setting_package_byte_array = util.PassPharseXOR(Encoding.UTF8.GetBytes(setting + crcStr), _xorPass);

                byte[] headerByte1 = Encoding.UTF8.GetBytes("DOK");
                byte[] headerByte2 = Encoding.UTF8.GetBytes("REQ_SETTING");                
                
                ArrayList settingByteArray = new ArrayList();
                foreach (byte b in headerByte1)
                {
                    settingByteArray.Add(new MyByte(b));
                }
                settingByteArray.Add(new MyByte(10));
               
                foreach (byte b in headerByte2)
                {
                    settingByteArray.Add(new MyByte(b));
                }
                settingByteArray.Add(new MyByte(10));
               
                foreach (byte b in xor_setting_package_byte_array)
                {
                    settingByteArray.Add(new MyByte(b));
                }
                               
                byte[] sendByte = new byte[settingByteArray.Count + 1];  
                MyByte myByte;
                for (int i = 0; i < settingByteArray.Count; i++)
                {
                    myByte = settingByteArray[i] as MyByte;
                    sendByte[i] = myByte.TheByte;
                }
               
                sendByte[sendByte.Length - 1] = 0x0A;            

                response = util.SendMsgOverTCPConnectionToHub(_spGSM, sendByte, 5000);

                //send history data log but send failed or no response from HUB
                if (response.RESPONSE_CODE == HUB_RESPONSE_CODE.SEND_FAILED || response.RESPONSE_CODE == HUB_RESPONSE_CODE.NO_RESPONSE)     
                {
                    //Retry according to retries setting
                    if (retry_count < MAX_RETRIES)
                    {
                        response = util.SendMsgOverTCPConnectionToHub(_spGSM, sendByte, 5000);
                        retry_count++;
                        continue;
                    }
                }
                
                //set all varialbe to null for garbage collector to collect
                serial = null; fw_ver = null; name = null; transmit_int = null; ip = null; port = null; sim_number = null; apn_name = null; apn_username = null; apn_password = null;
                setting = null; 
                //setting_package = null; 
                //sendString = null; 
                util = null; response = null;               

                break;
            }
        }

        //Handle hub command for set setting, handle command SET_SETTING
        public static void HandleHubCommandForSetSetting(byte[] hub_reponse_byte_array)
        {
            if (hub_reponse_byte_array == null)
                return;

            HUtility util  = new HUtility();            
            HUB_RESPONSE response = new HUB_RESPONSE(HUB_RESPONSE_CODE.SEND_FAILED, null);
            string reply = null;

            //Start parsing response for settings
            string[] res_lines = util.ByteArrayToStringLines(hub_reponse_byte_array);
            //Check for proper response, should contain only 3 lines, HOK, SET_SETTING, PAYLOAD
            if (res_lines.Length < 3)
                return;

            string[] payload_lines = res_lines[2].Split('|');
            //Check for proper payload package, only 14 lines
            if (payload_lines.Length != 15)
                return;          

            //Verify CRC
            string org_crc_str = payload_lines[14];
            string setting_str = res_lines[2].Substring(0, res_lines[2].Length - 4);
            byte[] cal_crc = Crc16.ComputeBytes(Encoding.UTF8.GetBytes(setting_str));
            string cal_crc_str = HTools.ByteArr.ToHex(cal_crc);

            if (String.Compare(org_crc_str.ToLower(), cal_crc_str.ToLower()) != 0)
            {
                reply = "INVALID_CHECKSUM\n" + "SET_SETTING\n" + res_lines[2] + '\n';
                util.SendMsgOverTCPConnectionToHub(_spGSM, Encoding.UTF8.GetBytes(reply), 100);
                
                //set null for GC to collect
                reply = null; util = null; response = null; res_lines = null; payload_lines = null;
                return;
            }
            
            //Check for correct serial number
            string dev_serial = _HW_VER.ToString() + StringPadleft(_HW_SERIAL.ToString(), '0', 5);
            if (String.Compare(payload_lines[0].Trim(), dev_serial) != 0)
            {
                reply = "INVALID_SERIAL\n" + "SET_SETTING\n" + res_lines[2] + '\n';
                util.SendMsgOverTCPConnectionToHub(_spGSM, Encoding.UTF8.GetBytes(reply), 100);

                //set null for GC to collect
                reply = null; util = null; response = null; res_lines = null; payload_lines = null; dev_serial = null;
                return;
            }

            //Check for correct firmware version
            if (String.Compare(payload_lines[1].Trim(), _FW_VER.ToString()) != 0)
            {
                reply = "INVALID_FW_VER\n" + "SET_SETTING\n" + res_lines[2] + '\n';
                util.SendMsgOverTCPConnectionToHub(_spGSM, Encoding.UTF8.GetBytes(reply), 100);

                //set null for GC to collect
                reply = null; util = null; response = null; res_lines = null; payload_lines = null; dev_serial = null;
                return;
            }

            //Apply new setting by writing to settings.txt file    
            try
            {                
                //set to code variable
                _DEVICE_NAME = payload_lines[2].Trim();
                _TRANSMIT_INTERVAL = int.Parse(payload_lines[3].Trim());
                _wireless_sensor_offline_threshold = int.Parse(payload_lines[4].Trim());
                _ip = payload_lines[5].Trim();
                _port = payload_lines[6].Trim();                
                _apn_name = payload_lines[8].Trim();
                _apn_username = payload_lines[9].Trim();
                _apn_password = payload_lines[10].Trim();
                _epoch_time_stamp = payload_lines[11].Trim();
                _setting_id = payload_lines[12].Trim();
                _sensors_definition = payload_lines[13].Trim();

                //Registered newly sensors definition
                RegisterWiredAndWirelessSensorsConfiguration(_sensors_definition);

                //Overwrite settings.txt file    
                StringBuilder sb = new StringBuilder();                

                sb.AppendLine("PROCESS_GPS_DATA_INTERVAL = " + _PROCESS_GPS_DATA_INTERVAL.ToString());
                sb.AppendLine("DEVICE_NAME = " + _DEVICE_NAME);
                sb.AppendLine("TRANSMIT_INTERVAL = " + _TRANSMIT_INTERVAL.ToString());
                sb.AppendLine("WIRELESS_SENSOR_OFFLINE_THRESHOLD = " + _wireless_sensor_offline_threshold.ToString());
                sb.AppendLine("IP = " + _ip);
                sb.AppendLine("PORT = " + _port);
                sb.AppendLine("DEVICE_PHONE_NO = " + _SIMPhoneNo);
                sb.AppendLine("APN_NAME = " + _apn_name);
                sb.AppendLine("APN_USERNAME = " + _apn_username);
                sb.AppendLine("APN_PASSWORD = " + _apn_password);
                sb.AppendLine("EPOCH_TIME_STAMP = " + _epoch_time_stamp);
                sb.AppendLine("SETTING_ID = " + _setting_id);
                sb.AppendLine("SENSORS_DEFINITION = " + _sensors_definition);
                
                if (_IsRTCSet == true)
                {
                    sb.AppendLine("RTC_SET = 1");
                }
                
                _sdStor.WriteToSettingFile(sb.ToString(), false);

                //send reply to hub
                //Create setting package
                string serial = _HW_VER.ToString() + StringPadleft(_HW_SERIAL.ToString(), '0', 5);
                string fw_ver = _FW_VER.ToString();
                string name = _DEVICE_NAME;
                string transmit_int = _TRANSMIT_INTERVAL.ToString();  //convert to minutes
                string ip = _ip;
                string port = _port;
                string sim_number = _SIMPhoneNo;
                string apn_name = _apn_name;
                string apn_username = _apn_username;
                string apn_password = _apn_password;
                string offline_threshold = _wireless_sensor_offline_threshold.ToString();
                string setting_id = _setting_id;
                string sensors_definition = _sensors_definition;
                StringBuilder sdsb = new StringBuilder(_sensors_definition);
                sdsb.Replace("DEVICE_SERIAL", serial);

                string setting = serial + "|" + fw_ver + "|" + name + "|" + transmit_int + "|" + offline_threshold + "|" + ip + "|" + port + "|" + sim_number + "|" + _apn_name + "|" + apn_username + "|" +
                                 apn_password + "|" + _epoch_time_stamp + "|" + _setting_id + "|" + sdsb.ToString().Trim() + "|";

                //CRC computation
                byte[] crc = Crc16.ComputeBytes(Encoding.UTF8.GetBytes(setting));
                byte[] reverse_crc = new byte[2];
                reverse_crc[0] = crc[1];
                reverse_crc[1] = crc[0];
                string crcStr = HTools.ByteArr.ToHex(reverse_crc);
                string settingStr = HTools.ByteArr.ToHex(Encoding.UTF8.GetBytes(setting));
                //Debug.Print(settingStr);

                //Xor with passphrase
                byte[] xor_setting_package_byte_array = util.PassPharseXOR(Encoding.UTF8.GetBytes(setting + crcStr), _xorPass);

                byte[] headerByte1 = Encoding.UTF8.GetBytes("DOK");
                byte[] headerByte2 = Encoding.UTF8.GetBytes("SET_SETTING");

                ArrayList settingByteArray = new ArrayList();
                foreach (byte b in headerByte1)
                {
                    settingByteArray.Add(new MyByte(b));
                }
                settingByteArray.Add(new MyByte(10));

                foreach (byte b in headerByte2)
                {
                    settingByteArray.Add(new MyByte(b));
                }
                settingByteArray.Add(new MyByte(10));

                foreach (byte b in xor_setting_package_byte_array)
                {
                    settingByteArray.Add(new MyByte(b));
                }

                byte[] sendByte = new byte[settingByteArray.Count + 1];
                MyByte myByte;
                for (int i = 0; i < settingByteArray.Count; i++)
                {
                    myByte = settingByteArray[i] as MyByte;
                    sendByte[i] = myByte.TheByte;
                }

                sendByte[sendByte.Length - 1] = 0x0A;

                response = util.SendMsgOverTCPConnectionToHub(_spGSM, sendByte, 5000);                
               
                return;
            }
            catch
            {               
                return;
            }            
        }
        
        //COM3 send ack command for Point Six sensor to go to sleep
        public static void Send_PointSix_Sensor_ACK_CMD(string sensorSN)
        {
            if (sensorSN == null)
                return;

            if (String.Compare(sensorSN, "") == 0)
                return;

            if (_spCOM3.IsOpen == true)
            {
                HUtility util = new HUtility();
                util.WriteToUART(_spCOM3, HTools.Hex.ToBytes("C33C0020" + sensorSN.Trim() + "FFFFA6"), false);
                util = null;
            }
        } 
        
        //Get current wireless sensors reading
        public static string Get_Wireless_PointSix_Sensors_Reading()
        {
            StringBuilder sb = new StringBuilder();            
            foreach (var sensorObject in _PointSixSensorList)
            {
                if (sensorObject is SensorRecord)
                {
                    SensorRecord sensor = sensorObject as SensorRecord;
                    sb.AppendLine(sensor.TimeStamp.ToString() + ", " + sensor.SensorID + ", " + sensor.RawIOValue1.ToString() + ", " + sensor.RawIOValue2.ToString() + ", " + sensor.Battery.ToString());
                }
            }            
            return sb.ToString();
        }

        //Load settings from sd card
        public static void LoadSettingsFromSDCard()
        {
            HUtility util = new HUtility();   
            string[] settingsValue = _sdStor.ReadSettingFile();

            if (settingsValue == null)  //Error reading setting parameter from sd card, flash the led continuously
            {
                while (true)
                {
                    //Indicate unable loading settings from SD Card
                    util.BlinkLED(_statusLED, 1, 300, LED_Blink_Type.ON_OFF);
                    Thread.Sleep(3000);
                }
            }

            foreach (var item in settingsValue)
            {
                string[] keyValuePair = item.Split('=');
                if (keyValuePair.Length < 2)
                    continue;

                if (String.Compare("TRANSMIT_INTERVAL", keyValuePair[0].ToString().Trim()) == 0)
                {
                    //it is in minutes from settings file
                    _TRANSMIT_INTERVAL = int.Parse(keyValuePair[1].ToString().Trim());
                }
                else if (String.Compare("DEVICE_NAME", keyValuePair[0].ToString().Trim()) == 0)
                {
                    _DEVICE_NAME = keyValuePair[1].ToString().Trim();
                }
                else if (String.Compare("DEVICE_PHONE_NO", keyValuePair[0].ToString().Trim()) == 0)
                {
                    _SIMPhoneNo = keyValuePair[1].ToString().Trim();
                }
                else if (String.Compare("IP", keyValuePair[0].ToString().Trim()) == 0)
                {
                    _ip = keyValuePair[1].ToString().Trim();
                }
                else if (String.Compare("PORT", keyValuePair[0].ToString().Trim()) == 0)
                {
                    _port = keyValuePair[1].ToString().Trim();
                }
                else if (String.Compare("APN_NAME", keyValuePair[0].ToString().Trim()) == 0)
                {
                    _apn_name = keyValuePair[1].ToString().Trim();
                }
                else if (String.Compare("APN_USERNAME", keyValuePair[0].ToString().Trim()) == 0)
                {
                    _apn_username = keyValuePair[1].ToString().Trim();
                }
                else if (String.Compare("APN_PASSWORD", keyValuePair[0].ToString().Trim()) == 0)
                {
                    _apn_password = keyValuePair[1].ToString().Trim();
                }
                else if (String.Compare("WIRELESS_SENSOR_OFFLINE_THRESHOLD", keyValuePair[0].ToString().Trim()) == 0)
                {
                    _wireless_sensor_offline_threshold = int.Parse(keyValuePair[1].ToString().Trim());
                }
                else if (String.Compare("EPOCH_TIME_STAMP", keyValuePair[0].ToString().Trim()) == 0)
                {
                    _epoch_time_stamp = keyValuePair[1].ToString().Trim();
                }
                else if (String.Compare("SETTING_ID", keyValuePair[0].ToString().Trim()) == 0)
                {
                    _setting_id = keyValuePair[1].ToString().Trim();
                }
                else if (String.Compare("SENSORS_DEFINITION", keyValuePair[0].ToString().Trim()) == 0)
                {
                    _sensors_definition = keyValuePair[1].ToString().Trim();                    
                }
                else if (String.Compare("RTC_SET", keyValuePair[0].ToString().Trim()) == 0)
                {
                    if (int.Parse(keyValuePair[1].ToString().Trim()) == 1)
                    {
                        _IsRTCSet = true;
                        //Set system local time
                        RTC rtc = new RTC();
                        Utility.SetLocalTime(rtc.GetRTC());
                        rtc = null;
                    }
                    else
                    {
                        _IsRTCSet = false;
                    }
                }
                else
                {
                    continue;
                }
            }           
        }

        //Load incomplete download list if any
        public static void LoadIncompleteDownloadList()
        {
            _IncompleteDownloadList.Clear();
            string[] packages = _sdStor.ReadIncompleteDownloadListFromFile();

            if (packages == null)
                return;

            if (packages.Length <= 0)
                return;

            string[] data = null;
            foreach (var item in packages)
            {
                data = item.Split(',');
                if (data == null)
                    continue;
                if (data.Length != 15) //not the proper length, expect 15 data values
                    continue;
                
                _IncompleteDownloadList.Add(new IncompleteDownloadRecord(data[0], 
                                            new DateTime(Convert.ToInt32(data[1]), Convert.ToInt32(data[2]),Convert.ToInt32(data[3]),Convert.ToInt32(data[4]),Convert.ToInt32(data[5]),Convert.ToInt32(data[6])),
                                            new DateTime(Convert.ToInt32(data[7]), Convert.ToInt32(data[8]),Convert.ToInt32(data[9]),Convert.ToInt32(data[10]),Convert.ToInt32(data[11]),Convert.ToInt32(data[12])),                                          
                                            Convert.ToInt32(data[13]), Convert.ToInt32(data[14])));
            }
        }

        //Save any incomplete record to file
        public static void SaveIncompleteListToFile()
        {
            if (_IncompleteDownloadList.Count > 0)
            {
                _sdStor.WriteIncompleteDownloadListToFile(_IncompleteDownloadList);
            }
        }

        /// <summary>
        /// Update the database of sensors according to the sensors definition given
        /// </summary>
        /// <param name="sensorsDefinition"></param>
        public static void RegisterWiredAndWirelessSensorsConfiguration(string sensorsDefinition)
        {
            _PointSixSensorList.Clear();
            HUtility util = new HUtility();
            try
            {
                RTC rtc = new RTC();
                string[] sensorList = sensorsDefinition.Split(new char[] { '$' });

                foreach (var sensor in sensorList)
                {
                    string[] sensorDetail = sensor.Split(new char[] { '*' });
                    if (String.Compare(sensorDetail[0].Trim(), "ps") == 0)
                    {
                        string[] ioDetail = sensorDetail[4].Split(new char[] { ',' });
                        if (ioDetail.Length == 2)
                        {
                            _PointSixSensorList.Add(new SensorRecord(sensorDetail[3].Trim(), 0, rtc.GetRTC(), 0, 0, "N/A", 104, 0, -1, -1, -1, 3));
                        }
                        else if (ioDetail.Length == 1)
                        {
                            string[] ioDetail1 = ioDetail[0].Split(new char[] { ':' });
                            if (String.Compare(ioDetail1[0].Trim(), "io1") == 0)
                            {
                                _PointSixSensorList.Add(new SensorRecord(sensorDetail[3].Trim(), 0, rtc.GetRTC(), 0, 0, "N/A", 104, 0, -1, -1, -1, 1));
                            }
                            else
                            {
                                _PointSixSensorList.Add(new SensorRecord(sensorDetail[3].Trim(), 0, rtc.GetRTC(), 0, 0, "N/A", 104, 0, -1, -1, -1, 2));
                            }
                        }
                        else
                        {
                            //Error in sensors' configuration
                        }
                    }
                    else if (String.Compare(sensorDetail[0].Trim(), "gprs") == 0)
                    {
                        string[] gprsIOList = sensorDetail[4].Split(new char[] { ',' });
                        for (int i = 0; i < gprsIOList.Length; i++)
                        {
                            string[] ioDetail = gprsIOList[i].Split(new char[] { ':' });
                            //add to _registeredWiredPorts array list the gprs port
                            _PointSixSensorList.Add(new GPRSPort(ioDetail[0].Trim()));
                        }
                    }
                    else
                    {
                        //ignore the damn thing
                    }
                }

                //Check to see if there are any sensors in the list first, enter sensors data into database if it is not empty
                if (_PointSixSensorList.Count <= 0)
                    return;
                
                //Insert to SQLite Database
                DB db = new DB();
                SensorRecord sr = null;
                GPRSPort port_io = null;
                if (db.OpenDB() == true)
                {
                    //Clear all tables
                    if (db.TruncateDBTables() == true)
                    {
                        foreach (var sensor in _PointSixSensorList)
                        {
                            if (sensor is SensorRecord)
                            {
                                sr = sensor as SensorRecord;
                                db.InsertSensorRecord(sr);
                            }
                            else if (sensor is GPRSPort)
                            {
                                port_io = sensor as GPRSPort;
                                db.InsertPortRecord(port_io);
                            }
                        }
                    }
                }
                db.CloseDB();                  
            }
            catch
            {
                //Need system reset
                SpecialGlobalVar._SystemNeedReboot = true;

                while (true)
                {
                    util.BlinkLED(_statusLED, 1, 300, LED_Blink_Type.ON_OFF);
                }
            }
        }

        public static void TransmitDeviceDataPackage()
        {            
            HUtility util = new HUtility();


            ///////////////////////Test Code////////////////
            DateTime rtc = RealTimeClock.GetDateTime();
            DateTime systemTime = DateTime.Now;

            util.WriteToUART(_spGSM, "RTC: " + rtc.ToString(), true);
            util.WriteToUART(_spGSM, "System Time: " + systemTime.ToString(), true);
            ////////////////////////////////////////////////


            byte[] byte_dataPackage = new byte[] { };
            //Get IOs readings 
            IOData ioData = GetIOReadings();

            //Create data package to send
            lock (_GPSDataObject)
            {
                //Get byte package containing wired and wireless data according to the configuration
                byte_dataPackage = CreateBytesPackageToSend(ioData);
            }

            if (_GSMUnitOnline)
            {                
                //Open TCP Session and send message
                if (util.OpenTCPSession(_spGSM, _ip, _port) == true)
                {
                    //Successfully connected to the hub through TCP, reset "SpecialGlobalVar._TCPConnectionRetries" variable
                    SpecialGlobalVar._TCPConnectionRetries = 0;

                    /////////////////////////////////////////////////////////////////////////////////
                    //If system just reset or reboot by the Watchdog timer, send setting to HUB
                    //
                    //put watch dog code here to detect watchdog just reset the system                    
                    /////////////////////////////////////////////////////////////////////////////////


                    ////////////////////////////
                    ////Test code
                    //string psSensors = Get_Wireless_PointSix_Sensors_Reading();
                    //util.SendMsgOverTCPConnection(_spGSM, psSensors);
                    //////////////////////////////////////////////////////


                    //Turn on transmission LED
                    _transmissionLED.Write(true);                    


                    //Send data over TCP Connection to HUB                       
                    HUB_RESPONSE hub_response = util.SendMsgOverTCPConnectionToHub(_spGSM, byte_dataPackage, 5000);

                    //Failed send or no response from HUB, store undeliverable data back to files
                    if (hub_response.RESPONSE_CODE == HUB_RESPONSE_CODE.SEND_FAILED)
                    {
                        //Record the package that was failed to deliver                           
                        ArrayList dataPackageHexStr = new ArrayList();
                        dataPackageHexStr.Add(util.ByteArrayToHexString(byte_dataPackage));
                        _sdStor.WriteUndeliverableDataToDataLogFile(dataPackageHexStr, _LogFileIndex, out _LogFileIndex, MAX_LOG_FILE_SIZE, LOG_FILE_TYPE.REGULAR_LOG_FILE);
                    }

                    if (hub_response.RESPONSE_CODE == HUB_RESPONSE_CODE.OK_WITH_NO_ACTION_COMMAND) //OK response from HUB with no Action Command, start sending history data if there are any       
                    {
                        UploadHistoryLog(LOG_FILE_TYPE.REGULAR_LOG_FILE);
                        UploadHistoryLog(LOG_FILE_TYPE.SENSOR_DOWNLOAD_LOG_FILE);
                    }

                    if (hub_response.RESPONSE_CODE == HUB_RESPONSE_CODE.OK_WITH_REQUEST_SETTING)  //Hub response OK with requesting device settings
                    {
                        //Handle HUB request for Setting
                        HandleHubRequestForSetting();
                    }

                    if (hub_response.RESPONSE_CODE == HUB_RESPONSE_CODE.OK_WITH_SET_SETTING) //Hub response OK with requesting device to set its settings according to the Hub payload
                    {
                        //Set settings according to Hub
                        HandleHubCommandForSetSetting(hub_response.ResponseByteArray);
                    }

                    if (hub_response.RESPONSE_CODE == HUB_RESPONSE_CODE.NOT_OK)
                    {
                        //ignore, don't store undeliverable data back to files
                        //Wait for future implementation of what to do when this happens
                        UploadHistoryLog(LOG_FILE_TYPE.REGULAR_LOG_FILE);
                        UploadHistoryLog(LOG_FILE_TYPE.SENSOR_DOWNLOAD_LOG_FILE);
                    }

                    //set variable to null for GC to collect
                    hub_response = null;

                    //Close the TCP session
                    if (util.CloseTCPSession(_spGSM) == false)
                    {
                        //Try once more only
                        util.CloseTCPSession(_spGSM);
                    }
                    
                    //Turn off transmission LED
                    _transmissionLED.Write(false);
                }
                else
                {
                    //keep track of how many failed attempts so we can normal power down the GSM unit inside "InitializeGSMUnit()" method
                    SpecialGlobalVar._TCPConnectionRetries++;

                    //try to close the TCP session                        
                    if (util.CloseTCPSession(_spGSM) == false)
                    {
                        //Try once more only
                        util.CloseTCPSession(_spGSM);
                    }

                    //Record the package that was failed to deliver                           
                    ArrayList dataPackageHexStr = new ArrayList();
                    dataPackageHexStr.Add(util.ByteArrayToHexString(byte_dataPackage));
                    _sdStor.WriteUndeliverableDataToDataLogFile(dataPackageHexStr, _LogFileIndex, out _LogFileIndex, MAX_LOG_FILE_SIZE, LOG_FILE_TYPE.REGULAR_LOG_FILE);

                    //check to see if GSM unit is online by intialize it
                    InitializeGSMUnit();
                }

                Debug.GC(true);
            }           
        }


        #endregion



        /////////////////////////////////////////////
        // PointSix Wireless Sensor Helper Methods //
        /////////////////////////////////////////////

        #region  [PointSix Wireless Sensor Helper Methods]

        public static void DownloadHistoryRecordsAndSendItToHub(SensorRecord beaconRecord, int[] memoryBlockList, DateTime startDate, DateTime endDate)
        {
            if (memoryBlockList == null)
                return;

            string[] historyPackageList = new String[memoryBlockList.Length];
            int historyPackageCount = 0;
            int lastAcquiredMemoryBlockListIndex = -1;        //keep track of the last memory block successfully acquired from the sensor so that we can determine what remaining
            string memLocation = null;
            string sensorSN = beaconRecord.SensorID;
            HUtility util = new HUtility();

            _stopWaiting = false;

            //Get the date and time of the start of the download for later use in parsing process.  This determine the time adjustment
            _sensorDownloadDateTime = DateTime.Now;

            //Start requesting memory blocks from the list  
            for (int i = 0; i < memoryBlockList.Length; i++)
            {
                if (_stopWaiting == true)     //Stop getting data, no reponse from sensor for the waited interval
                {
                    break;
                }

                //Memory location to get package                                       
                memLocation = memoryBlockList[i].ToString("X");

                //Send Get Log Data to sensor                                          
                _sendPacket = HTools.Hex.ToBytes(_REQ_DATA_CMD + sensorSN + "FF" + "00" + memLocation);
                util.WriteToUART(_spCOM3, _sendPacket, false);

                //Wait until you get the right data block back
                _ps_download_count_1 = 0;
                _ps_download_count_2 = 0;

                while (true)
                {
                    Thread.Sleep(1);
                    _ps_download_count_1++;
                    _ps_download_count_2++;

                    if (_ps_download_count_2 > 3000)
                    {
                        _stopWaiting = true;
                        break;
                    }


                    if (_NewPointSixPackageReceived)  //If there is a new package received
                    {
                        //SpecialGlobalVar._XBEE900SerialBuffer.CopyTo(_PointSixPackage, 0);
                        _NewPointSixPackageReceived = false;  //reset flag

                        string hexPackage = HTools.ByteArr.ToHex(SpecialGlobalVar._XBEE900SerialBuffer).ToUpper();

                        //Check to see if it is a Sensor Log Data Response                                          
                        if (hexPackage.Substring(6, 2) == "22")
                        {
                            //Check for match serial number, transaction id, and requested memory location                            
                            if (hexPackage.Substring(16, 2) == "FF" && hexPackage.Substring(8, 8) == _parsedSensorRecord.SensorID && hexPackage.Substring(20, 2) == memLocation)
                            {
                                //Add to dataList
                                historyPackageList[historyPackageCount] = HTools.ByteArr.ToHex(SpecialGlobalVar._XBEE900SerialBuffer);
                                historyPackageCount++;
                                lastAcquiredMemoryBlockListIndex = i;

                                ////////////////////Test Code //////////////////////////

                                Debug.Print("History: " + hexPackage);


                                ////////////////////////////////////////////////////////

                                break;
                            }
                        }
                    }

                    if (_ps_download_count_1 > 200)  //Resend command if requested package haven't comes in for a period of time
                    {
                        util.WriteToUART(_spCOM3, _sendPacket, false);
                        _ps_download_count_1 = 0;
                    }

                }//End while loop 

            }//End For Loop


            if (lastAcquiredMemoryBlockListIndex == (memoryBlockList.Length - 1))
            {
                //tell sensor to go to sleep, we got all required memory blocks
                Send_PointSix_Sensor_ACK_CMD(_parsedSensorRecord.SensorID, 3);
                Debug.Print("All memory blocks downloaded successfully.");


                //////////////////////////Test Code ///////////////////////////////
                SensorRecord srTest = null;
                Debug.Print("---------------------------------REGISTERED SENSORS LIST---------------------------");
                foreach (var sensor in _PointSixSensorList)
                {
                    srTest = sensor as SensorRecord;
                    Debug.Print(srTest.ToString() + "\n");
                }

                Debug.Print("---------------------------------END LISTS-----------------------------------------");
                //////////////////////////////////////////////////////////////////////
            }
            else
            {
                //Make sure the incomplete list doesn't contain

                int startMemBlock = memoryBlockList[lastAcquiredMemoryBlockListIndex + 1];
                int endMemBlock = memoryBlockList[memoryBlockList.Length - 1];
                _IncompleteDownloadList.Add(new IncompleteDownloadRecord(sensorSN, startDate, endDate, startMemBlock, endMemBlock));

                ///////////////////Test Code//////////////////////////////
                string rem_mem = null;
                for (int i = lastAcquiredMemoryBlockListIndex + 1; i < memoryBlockList.Length; i++)
                {
                    rem_mem += memoryBlockList[i].ToString("X") + ", ";
                }
                Debug.Print("Remaining memory blocks: " + rem_mem);

                IncompleteDownloadRecord ir = null;
                Debug.Print("---------------------------------INCOMPLETE SENSORS LIST---------------------------");
                foreach (var record in _IncompleteDownloadList)
                {
                    ir = record as IncompleteDownloadRecord;
                    Debug.Print(ir.ToString() + "\n");
                }

                Debug.Print("---------------------------------END INCOMPLETE LIST-------------------------------");

                SensorRecord srTest = null;
                Debug.Print("---------------------------------REGISTERED SENSORS LIST---------------------------");
                foreach (var sensor in _PointSixSensorList)
                {
                    srTest = sensor as SensorRecord;
                    Debug.Print(srTest.ToString() + "\n");
                }

                Debug.Print("---------------------------------END LISTS-----------------------------------------");


                ///////////////////////////////////////////////////////////
            }

            //Parse the downloaded packages into SensorRecord object Queue
            Queue ParsedRecordQueue = ParseDownloadPackages(historyPackageList, startDate, endDate, beaconRecord);
            

            //Start sending download 
            if (ParsedRecordQueue != null)
            {
                if (ParsedRecordQueue.Count > 0)
                {
                    //Wait until GSM unit is free
                    while (SpecialGlobalVar._TCPTransmissionInProcess == true)
                    { }
                    //Parse the downloaded packages into hex string to create Download Data Package
                    Queue DownloadDataPackageQueue = CreateDownloadLogBytePackagesForPointSixSensor(ParsedRecordQueue, 24);

                    SpecialGlobalVar._TCPTransmissionInProcess = true;  //set flag

                    SendDownloadSensorPackage(DownloadDataPackageQueue);

                    SpecialGlobalVar._TCPTransmissionInProcess = false; //reset flag
                }
            }

            //Set variables to null for garbage collector            
            util = null;
            memLocation = null;
            Array.Clear(memoryBlockList, 0, memoryBlockList.Length);
            memoryBlockList = null;
            Array.Clear(historyPackageList, 0, historyPackageList.Length);
            historyPackageList = null;

            Debug.GC(true);
        }

        public static void SendDownloadSensorPackage(Queue downloadDataPackageQueue)
        {            
            if (_GSMUnitOnline == true)
            {                
                HUtility util = new HUtility();
                
              
                //Open TCP Session and send message
                if (util.OpenTCPSession(_spGSM, _ip, _port) == true)
                {
                    //Successfully connected to the hub through TCP, reset "SpecialGlobalVar._TCPConnectionRetries" variable
                    SpecialGlobalVar._TCPConnectionRetries = 0;
                   
                    //Turn on transmission LED
                    _transmissionLED.Write(true);


                    HUB_RESPONSE hub_response = new HUB_RESPONSE(HUB_RESPONSE_CODE.NOT_OK, null);
                    ArrayList failedSendPackageBytesStringList = new ArrayList();
                    byte[] byte_dataPackage = null;
                    string currentPackageBytesString = null;
                    //Begin sending data over TCP Connection to HUB                       
                    while (downloadDataPackageQueue.Count > 0)
                    {
                        //Get the package byte string
                        currentPackageBytesString = downloadDataPackageQueue.Dequeue() as string;
                        //Convert package byte string to byte array
                        byte_dataPackage = util.HexStringToByteArray(currentPackageBytesString);

                        //Send the byte array package
                        hub_response = util.SendMsgOverTCPConnectionToHub(_spGSM, byte_dataPackage, 5000);                       
                      
                        //Failed send or no response from HUB, store package back onto a queue and exit loop
                        if (hub_response.RESPONSE_CODE == HUB_RESPONSE_CODE.SEND_FAILED)
                        {
                            //Record the package that was failed to deliver to the queue, record the byte string not the byte array                           
                            failedSendPackageBytesStringList.Add(currentPackageBytesString);
                            break;
                        }

                        //OK response from HUB with no Action Command, continue upload from Queue    
                        if (hub_response.RESPONSE_CODE == HUB_RESPONSE_CODE.OK_WITH_NO_ACTION_COMMAND)   
                        {
                            continue;
                        }

                        //Hub response OK with requesting device settings, handle the request then continue
                        if (hub_response.RESPONSE_CODE == HUB_RESPONSE_CODE.OK_WITH_REQUEST_SETTING)  
                        {
                            //Handle HUB request for Setting
                            HandleHubRequestForSetting();
                            continue;
                        }

                        //Hub response OK with requesting device to set its settings according to the Hub payload, handle request then continue
                        if (hub_response.RESPONSE_CODE == HUB_RESPONSE_CODE.OK_WITH_SET_SETTING) 
                        {
                            //Set settings according to Hub
                            HandleHubCommandForSetSetting(hub_response.ResponseByteArray);
                            continue;
                        }

                        //Ignore, don't store undeliverable data back onto a queue and continue;                        
                        if (hub_response.RESPONSE_CODE == HUB_RESPONSE_CODE.NOT_OK)
                        {
                            continue;
                        }

                    }//end while loop

                    //set variable to null for GC to collect
                    hub_response = null;

                    //Close the TCP session
                    if (util.CloseTCPSession(_spGSM) == false)
                    {
                        //Try once more only
                        util.CloseTCPSession(_spGSM);
                    }

                    //Turn off transmission LED
                    _transmissionLED.Write(false);

                    //Check to see if there are any packages that were not send, must store to SD Card
                    if (failedSendPackageBytesStringList.Count > 0)
                    {
                        _sdStor.WriteUndeliverableDataToDataLogFile(failedSendPackageBytesStringList, _DownloadLogFileIndex, out _DownloadLogFileIndex, MAX_LOG_FILE_SIZE, LOG_FILE_TYPE.SENSOR_DOWNLOAD_LOG_FILE);
                    }

                    //Check to see if the queue still have any package left, must store into a file as well
                    if (downloadDataPackageQueue.Count > 0)
                    {
                        ArrayList leftOverPackageList  = new ArrayList();
                        for (int i = 0; i < downloadDataPackageQueue.Count; i++)
                        {
                            leftOverPackageList.Add(downloadDataPackageQueue.Dequeue());
                        }

                        _sdStor.WriteUndeliverableDataToDataLogFile(leftOverPackageList, _DownloadLogFileIndex, out _DownloadLogFileIndex, MAX_LOG_FILE_SIZE, LOG_FILE_TYPE.SENSOR_DOWNLOAD_LOG_FILE);
                    }
                }
                else
                {
                    //keep track of how many failed attempts so we can normal power down the GSM unit inside "InitializeGSMUnit()" method
                    SpecialGlobalVar._TCPConnectionRetries++;

                    //try to close the TCP session                        
                    if (util.CloseTCPSession(_spGSM) == false)
                    {
                        //Try once more only
                        util.CloseTCPSession(_spGSM);
                    }

                    //Check to see if the queue still have any packages, must store to SD Card
                    if (downloadDataPackageQueue.Count > 0)
                    {
                        ArrayList leftOverPackageList = new ArrayList();
                        for (int i = 0; i < downloadDataPackageQueue.Count; i++)
                        {
                            leftOverPackageList.Add(downloadDataPackageQueue.Dequeue());
                        }

                        _sdStor.WriteUndeliverableDataToDataLogFile(leftOverPackageList, _DownloadLogFileIndex, out _DownloadLogFileIndex, MAX_LOG_FILE_SIZE, LOG_FILE_TYPE.SENSOR_DOWNLOAD_LOG_FILE);
                    }   
                    
                    //check to see if GSM unit is online by intialize it
                    InitializeGSMUnit();
                }

                Debug.GC(true);
            } 


        }

        public static SensorRecord ParsePackage(byte[] byteData)
        {
            try
            {
                HUtility util = new HUtility();
                byte[] sanitizedBytes = null;
                string serialNo = GetSensorSerialNumber(byteData);
                if (serialNo == null)
                    return null;

                //Get sensor type here...
                byte[] sType = new byte[2];
                for (int i = 0; i < 2; i++)
                {
                    sType[i] = byteData[i + 34];
                }
                sanitizedBytes = util.SanitizeForUTF8(sType);                
                string sensorTypeCode = new string(Encoding.UTF8.GetChars(sanitizedBytes, 0, sanitizedBytes.Length));


                DateTime currentDateTime = DateTime.Now;
                //Get sensor clock
                byte[] sClk = new byte[4];
                sClk[0] = byteData[24]; sClk[1] = byteData[25]; sClk[2] = byteData[26]; sClk[3] = byteData[27];
                int sensorClk = Convert.ToInt32(HTools.ByteArr.ToHex(sClk), 16);
                _timeAdjustment = currentDateTime.AddSeconds(sensorClk * (-1));


                //Get the two IOs, they store opposite for RTD single and RTD dual.  First analog value is channel 2 and second analog value is channel 1
                int ioValue1 = 0, ioValue2 = 0;
                byte[] IOValue1ByteArray = new byte[4];
                byte[] IOValue2ByteArray = new byte[4];
                for (int i = 0; i < 4; i++)
                {
                    IOValue2ByteArray[i] = byteData[i + 48];
                }
                for (int i = 0; i < 4; i++)
                {
                    IOValue1ByteArray[i] = byteData[i + 52];
                }
                sanitizedBytes = util.SanitizedForValidHexBytes(IOValue1ByteArray);
                if (sanitizedBytes == null)
                    return null;

                string ioValue1HexStr = new string(Encoding.UTF8.GetChars(sanitizedBytes, 0, sanitizedBytes.Length));
                ioValue1 = Convert.ToInt32(ioValue1HexStr, 16);

                sanitizedBytes = util.SanitizedForValidHexBytes(IOValue2ByteArray);
                if (sanitizedBytes == null)
                    return null;

                string ioValue2HexStr = new string(Encoding.UTF8.GetChars(sanitizedBytes, 0, sanitizedBytes.Length));
                ioValue2 = Convert.ToInt32(ioValue2HexStr, 16);

                //Get Alarm status
                int alarmStatus = Int32.Parse(byteData[72].ToString());

                ////Get package origin
                //string packageOrigin = GetPackageOrigin(byteData);

                //Get the transmit period in seconds
                byte[] byteTransmitPeriod = new byte[2];
                for (int i = 0; i < 2; i++)
                {
                    byteTransmitPeriod[i] = byteData[i + 70];
                }
                string transmitPeriodHexStr = HTools.ByteArr.ToHex(byteTransmitPeriod);
                int transmitPeriod = Convert.ToInt32(transmitPeriodHexStr, 16);

                //Get battery life            
                byte[] byteBattPktCnt = new byte[3];
                byte[] byteMaxBattPktCnt = new byte[3];
                for (int i = 0; i < 3; i++)
                {
                    byteBattPktCnt[i] = byteData[i + 64];
                    byteMaxBattPktCnt[i] = byteData[i + 67];
                }
                string byteBattPktCntHexStr = HTools.ByteArr.ToHex(byteBattPktCnt);
                string byteMaxBattPktCntHexStr = HTools.ByteArr.ToHex(byteMaxBattPktCnt);
                double BattPktCnt = Convert.ToInt32(byteBattPktCntHexStr, 16);
                double MaxBattPktCnt = Convert.ToInt32(byteMaxBattPktCntHexStr, 16);
                int batteryLife = 0;

                //Check to see if battery usage information is supported
                if (transmitPeriod == 0 && MaxBattPktCnt == 0)
                {
                    batteryLife = 102; //Battery is not supported by this sensor
                }
                else
                {
                    batteryLife = (int)(100 - ((BattPktCnt / MaxBattPktCnt) * 100));
                    //Check to see if Battery life is within 0-100%
                    if (batteryLife < 0 || batteryLife > 100)
                        batteryLife = 101;
                }

                //Get the Block Index and Record Index
                int blockIndex = -1, recordIndex = -1;
                GetBlockIndexAndRecordIndex(byteData, out blockIndex, out recordIndex);

                return new SensorRecord(serialNo, transmitPeriod, currentDateTime, ioValue1, ioValue2, sensorTypeCode, batteryLife, alarmStatus, blockIndex, recordIndex, sensorClk, 0);
            }
            catch
            {
                //Swallow exception               
                Debug.GC(true);
                return null;
            }
        }

        public static Queue ParseDownloadPackages(string[] historyPackageList, DateTime startDate, DateTime endDate, SensorRecord beaconRecord)
        {           
            Queue parsedRecordQueue = new Queue();
            //Get Sensor Clock
            _timeAdjustment = _sensorDownloadDateTime.AddSeconds(beaconRecord.SensorClock * (-1));

            //Find the sensor, get the ioFlag                          
            int ioflag = 0;
            for (int i = 0; i < _PointSixSensorList.Count; i++)
            {
                //Check to see if it's a wireless sensor object
                if (_PointSixSensorList[i] is SensorRecord)
                {
                    SensorRecord sr = _PointSixSensorList[i] as SensorRecord;
                    if (String.Compare(sr.SensorID, beaconRecord.SensorID) == 0)
                    {
                        ioflag = sr.IOFlag;
                        break;
                    }
                }
            }

            int timeHeader = 0, historyLogTimeOffset = 0, timeStamp = 0;
            int datacount = 0, ioValue1 = 0, ioValue2 = 0;
            string[] byteStrArray = new string[75];
            foreach (var dataStr in historyPackageList)
            {
                if (dataStr == null)        //string is null, continue
                    continue;
                if (String.Compare(dataStr, "") == 0)
                    continue;
                if (dataStr.Length != 150)  //does not contain 150 characters, continue
                    continue;

                datacount++;

                int incr = 0;
                for (int i = 0; i < 75; i++)
                {
                    byteStrArray[i] = dataStr.Substring(incr, 2).ToUpper();
                    incr = incr + 2;
                }

                //Check timeHeader to verify if there are data in the current block, continue, else get time header
                if (byteStrArray[11] == "FF")
                    continue;

                timeHeader = Convert.ToInt32(byteStrArray[11] + byteStrArray[12] + byteStrArray[13] + byteStrArray[14], 16);
                if (timeHeader == 6945450)
                    timeStamp = 0;
                else
                    timeStamp = 0;

                ////////////////////////////////////
                //  Start getting history records //
                ////////////////////////////////////

                //Go through record index for each memory block                   
                int indexCount = 0;
                //bool foundMatchDT = false;
                DateTime currentRecordTimeStamp;
                for (int i = 0; i < 12; i++)
                {
                    //Check if it is an alarm record, if so ignore
                    int isAlarm = Convert.ToInt32(byteStrArray[15 + indexCount], 16);
                    isAlarm = isAlarm & 1;
                    if (isAlarm == 1)
                    {
                        indexCount += 5;
                        continue;
                    }

                    //Get current record time stamp
                    historyLogTimeOffset = Convert.ToInt32(byteStrArray[15 + indexCount], 16) * 30;
                    timeStamp += historyLogTimeOffset;
                    currentRecordTimeStamp = _timeAdjustment.AddSeconds(timeHeader + timeStamp);

                    //Get ioValue1 and ioValue2
                    ioValue1 = Convert.ToInt32(byteStrArray[16 + indexCount] + byteStrArray[17 + indexCount], 16);
                    ioValue2 = Convert.ToInt32(byteStrArray[18 + indexCount] + byteStrArray[19 + indexCount], 16);

                    //add only record that is between start and end date timeline
                    if (currentRecordTimeStamp >= startDate && currentRecordTimeStamp <= endDate)
                    {
                        parsedRecordQueue.Enqueue(new SensorRecord(beaconRecord.SensorID, historyLogTimeOffset, currentRecordTimeStamp, ioValue1, ioValue2, beaconRecord.Type,
                                                                  0, 0, 0, 0, beaconRecord.SensorClock, ioflag));
                    }

                    indexCount += 5;
                }
            }

            return parsedRecordQueue;
        }

        public static string GetSensorSerialNumber(byte[] byteData)
        {
            try
            {
                //Get Sensor Serial Number
                HUtility util = new HUtility();
                byte[] sanitizedBytes = null;
                byte[] SensorSN = new byte[8];
                for (int i = 0; i < 8; i++)
                { SensorSN[i] = byteData[i + 36]; }
                sanitizedBytes = util.SanitizePointSixSerialNoByteArray(SensorSN);

                if (sanitizedBytes == null) //invalid serial number
                    return null;

                string serialNo = new string(Encoding.UTF8.GetChars(sanitizedBytes, 0, sanitizedBytes.Length));
                if (serialNo == null)
                    return null;
                if (serialNo.Length < 8)
                    return null;

                return serialNo.Trim();
            }
            catch
            {
                return null;
            }
        }

        public static void Send_PointSix_Sensor_ACK_CMD(string sensorSN, int repeat)
        {
            if (sensorSN == null)
                return;

            if (String.Compare(sensorSN, "") == 0)
                return;

            if (_spCOM3.IsOpen == true)
            {
                HUtility util = new HUtility();
                for (int i = 0; i < repeat; i++)
                {
                    util.WriteToUART(_spCOM3, HTools.Hex.ToBytes("C33C0020" + sensorSN.Trim() + "FFFFA6"), false);
                    Thread.Sleep(200);
                }
                util = null;
            }
        }

        public static void GetBlockIndexAndRecordIndex(byte[] logNextPointer, out int blockIndex, out int recordIndex)
        {
            //Pre-defined by Point Six manufacture, formula for getting block index and the next record index in the block is
            //  iBlock = Log Next/LOGBLOCKSIZE
            //  iRecord = ((Log Next % LOGBLOCKSIZE) - LOGBLOCKTIMEHEADERSIZE) / LOGPACKETSIZE
            //Log Next should be masked with 3FFF before usage

            int LOGBLOCKSIZE = 64;      // 64 Bytes
            int LOGBLOCKTIMEHEADERSIZE = 4;     // 4 Bytes
            int LOGPACKETSIZE = 5;       //5 Bytes

            //Get the "Log Next Pointer"   
            byte[] logNP = new byte[2];
            logNP[0] = logNextPointer[28];
            logNP[1] = logNextPointer[29];
            string logNPHexString = HTools.ByteArr.ToHex(logNP);


            //Check for End of Memory Transistion
            if (String.Compare(logNPHexString, "4000") == 0)    // Pointing to the first block in the log, just wrapped
            {
                blockIndex = 0;
                recordIndex = 0;
                return;
            }

            //Mask "Log Next Pointer" with 3FFF
            int logNextDecimal = Convert.ToInt32(logNPHexString, 16);
            int maskValue = Convert.ToInt32("3FFF", 16);
            int logNextValue = logNextDecimal & maskValue;

            blockIndex = logNextValue / LOGBLOCKSIZE;
            recordIndex = ((logNextValue % LOGBLOCKSIZE) - LOGBLOCKTIMEHEADERSIZE) / LOGPACKETSIZE;

        }

        public static string GetPackageOrigin(byte[] bytesArray)
        {
            HUtility util = new HUtility();
            //Get package origin, who sent this package?  0-Wifi, 1-Point Manager, 2-Ethernet Point Repeater, 3-Application, 4-Wifi Repeater, 5-Counter Manager, 6-Wifi Sensor PassThru(Point Manager),
            //7-Link Manager, 8-Wifi Repeater Logger, 9-Wifi Repeater Alarm
            string packageOrigin = "";
            string abyte = bytesArray[63].ToString();
            int packageOriginCode = Int32.Parse(abyte);


            switch (packageOriginCode)
            {
                case 0:
                    packageOrigin = "WiFi/Direct from Sensor";
                    break;
                case 1:
                    packageOrigin = "Point Manager";
                    break;
                case 2:
                    packageOrigin = "Ethernet Point Repeater";
                    break;
                case 3:
                    packageOrigin = "Application";
                    break;
                case 4:
                    packageOrigin = "WiFi Repeater";
                    break;
                case 5:
                    packageOrigin = "Counter Manager";
                    break;
                case 6:
                    packageOrigin = "WiFi Sensor PassThru(Point Manager)";
                    break;
                case 7:
                    packageOrigin = "Link Manager";
                    break;
                case 8:
                    packageOrigin = "WiFi Repeater Logger";
                    break;
                case 9:
                    packageOrigin = "WiFi Repeater Alarm";
                    break;
                default:
                    packageOrigin = "Unknown";
                    break;
            }
            return packageOrigin;
        }

        public static ArrayList GetMemoryList(int startBlock, int endBlock)
        {
            ArrayList memList = new ArrayList();

            //Base cases
            if (startBlock == endBlock)
            {
                memList.Add(startBlock);
                return memList;
            }

            for (int i = startBlock; i <= endBlock; i++)
            {
                memList.Add(i);
            }

            return memList;
        }

        public static int[] GetMemoryListForDownload(SensorRecord newRecord, SensorRecord oldRecord, int oldestRecord)
        {
            //Current sensor clock is less than previous sensor clock, must request for oldest log record and collect from there, if this is the case oldestRecord is not -1    
            if (oldestRecord > -1)  //must download from oldest to current block
            {
                //If oldest record is FF=255, then just add it to the beginning of the list, then add block 0 to current block
                if (oldestRecord == 255)
                {
                    ArrayList memArray = GetMemoryList(0, newRecord.BlockIndex);
                    memArray.Add(255);
                    Array memList = Array.CreateInstance(typeof(int), memArray.Count);
                    memArray.CopyTo(memList, 0);
                    return (int[])memList;
                }
                else if (oldestRecord > newRecord.BlockIndex)   //we must create two list, one from oldest record to 255, and another from 0 to current block
                {
                    ArrayList memArray1 = GetMemoryList(oldestRecord, 255);
                    ArrayList memArray2 = GetMemoryList(0, newRecord.BlockIndex);
                    Array memList = Array.CreateInstance(typeof(int), memArray1.Count + memArray2.Count);
                    memArray1.CopyTo(memList, 0);
                    memArray2.CopyTo(memList, memArray1.Count);
                    return (int[])memList;
                }
                else //this is when the oldest record is less then the current record memory block
                {
                    ArrayList memArray = GetMemoryList(oldestRecord, newRecord.BlockIndex);
                    Array memList = Array.CreateInstance(typeof(int), memArray.Count);
                    memArray.CopyTo(memList, 0);
                    return (int[])memList;
                }
            }

            //Memory rolled over, generate 2 sequences, from sr.BlockIndex to FF = 255 block, and from 0 to current block index
            if (oldRecord.BlockIndex > newRecord.BlockIndex)
            {
                ArrayList memArray1 = GetMemoryList(oldRecord.BlockIndex, 255);
                ArrayList memArray2 = GetMemoryList(0, newRecord.BlockIndex);
                Array memList = Array.CreateInstance(typeof(int), memArray1.Count + memArray2.Count);
                memArray1.CopyTo(memList, 0);
                memArray2.CopyTo(memList, memArray1.Count);
                return (int[])memList;
            }
            else //Same block index or normal case            
            {
                ArrayList memArray = GetMemoryList(oldRecord.BlockIndex, newRecord.BlockIndex);
                Array memList = Array.CreateInstance(typeof(int), memArray.Count);
                memArray.CopyTo(memList, 0);
                return (int[])memList;
            }
        }

        public static void UpdateSensorRecord(SensorRecord newRecord, SensorRecord oldRecord)
        {
            try
            {
                oldRecord.TimeStamp = DateTime.Now;
                oldRecord.RawIOValue1 = newRecord.RawIOValue1;
                oldRecord.RawIOValue2 = newRecord.RawIOValue2;
                oldRecord.SensorClock = newRecord.SensorClock;
                oldRecord.TransmitPeriod = newRecord.TransmitPeriod;
                oldRecord.BlockIndex = newRecord.BlockIndex;
                oldRecord.RecordIndex = newRecord.RecordIndex;
                oldRecord.Battery = newRecord.Battery;
                oldRecord.AlarmStatus = newRecord.AlarmStatus;                

                //////////////////////// Test Code ////////////////////
                SensorRecord srTest = null;
                Debug.Print("---------------------------------REGISTERED SENSORS LIST---------------------------");
                foreach (var sensor in _PointSixSensorList)
                {
                    srTest = sensor as SensorRecord;
                    Debug.Print(srTest.ToString() + "\n");
                }

                Debug.Print("---------------------------------END LISTS-----------------------------------------");
                /////////////////////////////////////////////////////////
            }
            catch
            {
                //Swallow it
            }
        }

        public static void UpdateSensorRecordToDatabase()
        {
            if (_PointSixSensorList.Count <= 0)
                return;

            //Update to database
            DB db = new DB();
            SensorRecord sr = null;
            if (db.OpenDB() == true)
            {
                foreach (var sensor in _PointSixSensorList)
                {
                    if (sensor is SensorRecord)
                    {
                        sr = sensor as SensorRecord;
                        db.UpdateSensorRecord(sr);
                    }
                }
                db.CloseDB();
            }            
        }

        public static int GetOldestBlock(string sensorSN)
        {
            HUtility util = new HUtility();
            _oldestBlock = -1;  //default
            //Send Get Log Data to sensor                                          
            _sendPacket = HTools.Hex.ToBytes(_REQ_DATA_CMD + sensorSN + "EE" + "FFFF");
            util.WriteToUART(_spCOM3, _sendPacket, false);

            //Wait until you get the right data block back
            _ps_download_count_1 = 0;
            _ps_download_count_2 = 0;

            while (true)
            {
                Thread.Sleep(1);
                _ps_download_count_1++;
                _ps_download_count_2++;

                if (_ps_download_count_2 > 3000)
                {
                    break;
                }

                if (_NewPointSixPackageReceived)  //If there is a new package received
                {
                    //SpecialGlobalVar._XBEE900SerialBuffer.CopyTo(_PointSixPackage, 0);
                    _NewPointSixPackageReceived = false;  //reset flag

                    string hexPackage = HTools.ByteArr.ToHex(SpecialGlobalVar._XBEE900SerialBuffer).ToUpper();

                    //Check to see if it is a Sensor Log Data Response, check for match serial number, transaction id, and requested memory location                                                
                    if (hexPackage.Substring(6, 2) == "22" && hexPackage.Substring(16, 2) == "EE" && hexPackage.Substring(8, 8) == sensorSN)
                    {
                        //Get oldest block
                        _oldestBlock = Convert.ToInt32(hexPackage.Substring(20, 2), 16);

                        Debug.Print("Oldest Block Acquired: " + hexPackage);

                        break;
                    }
                }

                if (_ps_download_count_1 > 200)  //Resend command if requested package haven't comes in for a period of time
                {
                    util.WriteToUART(_spCOM3, _sendPacket, false);
                    _ps_download_count_1 = 0;
                }

            }//End while loop 

            if (_oldestBlock == -1)
                return -2;

            return _oldestBlock;
        }

        public static bool IsSensorDelay(SensorRecord oldRecord, SensorRecord newRecord, int delayInterval)
        {
            TimeSpan ts = newRecord.TimeStamp.Subtract(oldRecord.TimeStamp);
            int days_diff = ts.Days;
            int hours_diff = ts.Hours;
            int minutes_diff = ts.Minutes;

            if (days_diff > 0)   //Delay more than a day
                return true;
            else if (hours_diff > 0)  //Delay more than one hour
                return true;
            else if (minutes_diff > delayInterval)  //Delay more than specified minutes          
                return true;
            else
                return false;
        }

        public static long ToEpochTicks(DateTime dt){
            return dt.Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).Ticks / TimeSpan.TicksPerSecond;
        }

        #endregion

    }

    public class GPSData
    {
        public GPSData(DateTime utcDateTime, string longitude, string longitudeEW, string latitude, string latitudeNS, string speed)
        {
            this.UTCDateTime = utcDateTime;
            this.Longitude = longitude;
            this.LongitudeEW = longitudeEW;
            this.Latitude = latitude;
            this.LatitudeNS = latitudeNS;
            this.Speed = speed;
        }

        public DateTime UTCDateTime { get; set; }        
        public string Longitude { get; set; }
        public string LongitudeEW { get; set; }
        public string Latitude { get; set; }
        public string LatitudeNS { get; set; }
        public string Speed { get; set; }
    }

    public class IOData
    {
        public IOData(string temp_1, string temp_2, string on_off_1, string analog1)
        {
            this.Temp1 = temp_1;
            this.Temp2 = temp_2;
            this.OnOff1 = on_off_1;            
            this.Analog1 = analog1;           
        }

        public string Temp1 { get; set; }
        public string Temp2 { get; set; }
        public string OnOff1 { get; set; }       
        public string Analog1 { get; set; }        
    }        

    public class GPRSPort
    {
        public GPRSPort(string portName)
        {
            this.PortName = portName;            
        }
        public string PortName { get; set; }        
    }

    public class MyByte
    {
        public byte TheByte { get; set; }
        public MyByte(byte the_byte)
        {
            this.TheByte = the_byte;
        }
    }


    // io_flag is used to indicate which io is valid for sending
    // io_flag = 1, only io1 is valid 
    // io_flag = 2, only io2 is valid
    // io_flag = 3, io1 and io2 are both valid
    public class SensorRecord
    {
        public SensorRecord(string sensorID, int transmitPeriod, DateTime timeStamp, int raw_iovalue_1, int raw_iovalue_2, string type, int battery, int alarmStatus,
                         int blockIndex, int recordIndex, int sensorClock, int ioFlag)
        {
            this.SensorID = sensorID;
            this.TransmitPeriod = transmitPeriod;
            this.TimeStamp = timeStamp;
            this.RawIOValue1 = raw_iovalue_1;
            this.RawIOValue2 = raw_iovalue_2;
            this.Type = type;
            this.Battery = battery;
            this.AlarmStatus = alarmStatus;
            this.BlockIndex = blockIndex;
            this.RecordIndex = recordIndex;
            this.SensorClock = sensorClock;
            this.IOFlag = ioFlag;
        }

        public string SensorID { get; set; }
        public int TransmitPeriod { get; set; }
        public DateTime TimeStamp { get; set; }
        public int RawIOValue1 { get; set; }
        public int RawIOValue2 { get; set; }
        public string Type { get; set; }
        public int Battery { get; set; }
        public int AlarmStatus { get; set; }
        public int BlockIndex { get; set; }
        public int RecordIndex { get; set; }
        public int SensorClock { get; set; }
        public int IOFlag { get; set; }  //This is a flag to indicate which io is valid according to the configuration by the user

        public override string ToString()
        {
            return "SensorID: " + this.SensorID + ", " +
                   "TransmitPeriod: " + this.TransmitPeriod.ToString() + ", " +
                   "TimeStamp: " + this.TimeStamp.ToString() + ", " +
                   "RawIO1: " + this.RawIOValue1.ToString() + ", " +
                   "RawIO2: " + this.RawIOValue2.ToString() + ", " +
                   "Type: " + this.Type + ", " +
                   "Battery: " + this.Battery.ToString() + ", " +
                   "Alarm: " + this.AlarmStatus.ToString() + ", " +
                   "BlockIndex: " + this.BlockIndex.ToString() + ", " +
                   "RecordIndex: " + this.RecordIndex.ToString() + ", " +
                   "SensorClock: " + this.SensorClock.ToString() + ", " +
                   "IO Flag:" + this.IOFlag.ToString() + "\n";
        }
    }

    public class IncompleteDownloadRecord
    {
        public string SensorID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int StartMemBlock { get; set; }
        public int EndMemBlock { get; set; }
        public IncompleteDownloadRecord(string sensorID, DateTime startDate, DateTime endDate, int startMemBlock, int endMemBlock)
        {
            this.SensorID = sensorID;
            this.StartDate = startDate;
            this.EndDate = endDate;
            this.StartMemBlock = startMemBlock;
            this.EndMemBlock = endMemBlock;
        }

        public override string ToString()
        {
            return "SensorID: " + this.SensorID + ", " +
                   "StartDate: " + this.StartDate.ToString() + ", " +
                   "EndDate: " + this.EndDate.ToString() + ", " +
                   "StartMemBlock: " + this.StartMemBlock.ToString("X") + ", " +
                   "EndMemBlock: " + this.EndMemBlock.ToString("X");
        }
    }
}




 