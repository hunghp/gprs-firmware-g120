using System;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using GHI.Processor;

namespace GPS_GSM_Unit
{
    public class RTC
    {
        public RTC()
        { }
        
        public void SetRTC(DateTime dt)
        {                       
            RealTimeClock.SetDateTime(dt);                          
        }

        public DateTime GetRTC()
        {
            DateTime DT;
            try
            {
                DT = RealTimeClock.GetDateTime();
                return DT;
            }
            catch
            {
                DT = new DateTime(1970, 1, 1, 0, 0, 0);
                RealTimeClock.SetDateTime(DT);
                DT = RealTimeClock.GetDateTime();
                return DT;
            }                                              
        }                
    }
}
