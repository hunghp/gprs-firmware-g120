using System;
using System.Text;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using System.IO.Ports;
using System.Threading;
using GHI.Pins;

using HTools;

namespace GPS_GSM_Unit
{
    public enum LED_Blink_Type
	{
	    ON_OFF_ON = 0,
        ON_OFF = 1,
        OFF_ON = 2
	}

    public enum HUB_RESPONSE_CODE : int
    {
        OK_WITH_NO_ACTION_COMMAND = 0,
        NO_RESPONSE = 1,
        SEND_FAILED = 2,        
        OK_WITH_REQUEST_SETTING = 3,
        OK_WITH_SET_SETTING = 4,
        NOT_OK = 5
    }

    public class HUB_RESPONSE
    {
        public HUB_RESPONSE(HUB_RESPONSE_CODE reponseCode, byte[] reponseByteArray)
        {
            this.RESPONSE_CODE = reponseCode;
            this.ResponseByteArray = reponseByteArray;
        }

        public HUB_RESPONSE_CODE RESPONSE_CODE { get; set; }
        public byte[] ResponseByteArray { get; set; }
    }

    public class HUtility
    {
        public HUtility()
        { }

        public bool InitializeComPort(SerialPort sp)
        {
            //Open COM port
            try
            {
                if (!sp.IsOpen)
                {
                    sp.Open();                   
                    return true;
                }
                else if (sp.IsOpen)
                {
                    return true;
                }
                else 
                {
                    return false;
                }
            }
            catch
            {                
                return false;
            }            
        }

        public  void WakeGPS(OutputPort wakeGPSUpPin)
        {
            wakeGPSUpPin.Write(true);
            Thread.Sleep(1000);
            wakeGPSUpPin.Write(false);
            Thread.Sleep(5000);
        }

        public bool WakeUpGSMUnit(SerialPort sp)
        {
            //Look for "Call Ready" message
            byte[] byteMsg = SanitizeForUTF8(ReadUART(sp));

            if (byteMsg == null)
                return false;

            if (byteMsg.Length <= 0)
                return false;

            if (UARTMessageContain(byteMsg, "Call Ready") == true)
                return true;
            else
                return false;
        }

        public  void SendGPSSetupCmd(SerialPort sp)
        {
            try
            {
                string[] cmdShutOffNMEAMessages = new string[] { "$PSRF103,00,00,00,01*24", "$PSRF103,01,00,00,01*25", "$PSRF103,02,00,00,01*26",
                                                             "$PSRF103,03,00,00,01*27", "$PSRF103,05,00,00,01*21" };
                string cmdSetRMCMessageRate = "$PSRF103,04,00,02,01*22";

                foreach (string cmd in cmdShutOffNMEAMessages)
                {
                    WriteToUART(sp, cmd, true);
                    Thread.Sleep(1000);
                }
                WriteToUART(sp, cmdSetRMCMessageRate, true);
            }
            catch
            {
                return;
            }
        }

        public bool WriteToUART(SerialPort sp, string data, bool sendCRLF)
        {
            try
            {
                sp.Flush();
                byte[] byteData = Encoding.UTF8.GetBytes(data);
                sp.Write(byteData, 0, byteData.Length);
                if (sendCRLF)
                {
                    byte[] byteCRLF = new byte[] { 13, 10 };
                    sp.Write(byteCRLF, 0, byteCRLF.Length);
                    byteCRLF = null;
                }

                byteData = null;
                return true;
            }
            catch (Exception ex)
            {
                Debug.Print("From [WriteToUART (using string)]:" + ex.ToString());

                return false;
            }
        }

        public bool WriteToUART(SerialPort sp, byte[] data, bool sendCRLF)
        {
            if (data == null)
            {
                return false;
            }

            try
            {
                sp.Flush();
                sp.Write(data, 0, data.Length);                                               
                if (sendCRLF)
                {
                    byte[] byteCRLF = new byte[] { 13, 10 };
                    sp.Write(byteCRLF, 0, byteCRLF.Length);
                    byteCRLF = null;
                }
                return true;
            }
            catch (Exception ex)
            {
                Debug.Print("From [WriteToUART (using byte[] array)]: " + ex.ToString());
                sp.DiscardOutBuffer();
                sp.DiscardInBuffer();
                sp.Flush();
                return false;
            }
        }

        public  byte[] ReadUART(SerialPort sp)
        {            
            if (sp.BytesToRead <= 0)
                return null;           

            //byte[] buffer = new byte[1024];  //this causes System.OutOfMemoryException
            int byteRead = 0;
            int totalByteRead = 0;
            //byte[] buffer = null;
            try
            {
                // buffer = new byte[512];  //this causes System.OutOfMemoryException, move into the try catch clause                              
                while (sp.BytesToRead > 0)
                {
                    if (sp.PortName == "COM1")
                    {
                        if (totalByteRead >= SpecialGlobalVar._GPSSerialBuffer.Length)
                            break;

                        if (sp.BytesToRead > (SpecialGlobalVar._GPSSerialBuffer.Length - totalByteRead))
                            byteRead = sp.Read(SpecialGlobalVar._GPSSerialBuffer, totalByteRead, SpecialGlobalVar._GPSSerialBuffer.Length - totalByteRead);
                        else
                            byteRead = sp.Read(SpecialGlobalVar._GPSSerialBuffer, totalByteRead, sp.BytesToRead);
                    }
                    else
                    {
                        if (totalByteRead >= SpecialGlobalVar._GSMSerialBuffer.Length)
                            break;

                        if (sp.BytesToRead > (SpecialGlobalVar._GSMSerialBuffer.Length - totalByteRead))
                            byteRead = sp.Read(SpecialGlobalVar._GSMSerialBuffer, totalByteRead, SpecialGlobalVar._GSMSerialBuffer.Length - totalByteRead);
                        else
                            byteRead = sp.Read(SpecialGlobalVar._GSMSerialBuffer, totalByteRead, sp.BytesToRead);
                    }


                    totalByteRead += byteRead;
                }

                byte[] readBuffer = new byte[totalByteRead];
                
                if (sp.PortName == "COM1")
                    Array.Copy(SpecialGlobalVar._GPSSerialBuffer, 0, readBuffer, 0, totalByteRead);
                else
                    Array.Copy(SpecialGlobalVar._GSMSerialBuffer, 0, readBuffer, 0, totalByteRead);

                sp.Flush();
                //buffer = null;
                return readBuffer;
            }
            catch (Exception ex)
            {
                Debug.Print("From [ReadUART]: " + ex.ToString()); 

                sp.DiscardOutBuffer();
                sp.DiscardInBuffer();
                sp.Flush();

                //buffer = null;
                return null;
            }
        }

        public  bool IsGPSUnitOn(SerialPort sp)
        {
            byte[] resp = null;
            Thread.Sleep(2000);
            resp = SanitizeForUTF8(ReadUART(sp));
            if (UARTMessageContain(resp, "$") == true)
                return true;
            else
                return false;

            //try
            //{
            //    if (sp.BytesToRead > 1)
            //    {
            //        return true;
            //    }
            //    else
            //    {
            //        return false;
            //    }
            //}
            //catch
            //{
            //    return false;
            //}
        }

        public string StringXOR(string inputString)
        {
            byte[] byteStr = Encoding.UTF8.GetBytes(inputString);
            int xorDec = 0;

            for (int i = 0; i < byteStr.Length; i++)
            {
                xorDec = xorDec ^ byteStr[i];
            }

            return xorDec.ToString("X");
        }

        public byte[] PassPharseXOR(byte[] data, byte[] xorPhrase)
        {
            byte[] result = new byte[data.Length];

            for(var i=0; i < data.Length; i++)
	        {
                result[i] = (byte)(data[i] ^ xorPhrase[i % xorPhrase.Length]);
	        }
            return result;
        }

        public bool IsGSMUnitOnline(SerialPort sp)
        {
            //Look for "Call Ready" message
            byte[] byteMsg = SanitizeForUTF8(ReadUART(sp));

            if (byteMsg == null)
                return false;

            if (byteMsg.Length <= 0)
                return false;

            if (UARTMessageContain(byteMsg, "Call Ready") == true || UARTMessageContain(byteMsg, "RDY") == true)
                return true;
            else
                return false;                                    
        }

        public  void WakeGSM(OutputPort wakeGSMUnitPin)
        {
            wakeGSMUnitPin.Write(true);
            Thread.Sleep(1000);
            wakeGSMUnitPin.Write(false);
            Thread.Sleep(8000);
        }

        public  void WriteCTRL_Z(SerialPort sp)
        {
            byte[] Ctrl_Z = new byte[] { 26 };
            sp.Write(Ctrl_Z, 0, Ctrl_Z.Length);
        }
       
        //Return true if the uart byte array contain the string 'message'
        public  bool UARTMessageContain(byte[] uartByteArray, string message)
        {
            if (uartByteArray == null)
                return false;

            if (uartByteArray.Length <= 0)
                return false;
            
            try
            {
                string[] lines = ByteArrayToStringLines(uartByteArray);

                foreach (var line in lines)
                {
                    //check for string message                  
                    if (line.IndexOf(message) != -1)
                    {
                        lines = null;
                        return true;
                    }
                }

                lines = null;
                return false;
            }
            catch
            {
                return false;
            }           
        }

        //Check GSM Unit to see it's alive
        public  bool IsGSMUnitAlive(SerialPort sp)
        {            
            byte[] resp = null;
            WriteToUART(sp, "AT", true);
            Thread.Sleep(2000);
            resp = SanitizeForUTF8(ReadUART(sp));
            if (UARTMessageContain(resp, "OK") == false)
                return false;

            return true;
        }
        
        public void BlinkLED(OutputPort ledPin, int blinkCount, int blinkDelay, LED_Blink_Type blinkType )
        {
            for (int i = 0; i < blinkCount; i++)
            {
                if (blinkType == LED_Blink_Type.ON_OFF_ON)
                {
                    ledPin.Write(true);
                    Thread.Sleep(blinkDelay);
                    ledPin.Write(false);
                    Thread.Sleep(blinkDelay);
                    ledPin.Write(true);
                    Thread.Sleep(blinkDelay);
                }
                else if (blinkType == LED_Blink_Type.ON_OFF)
                {
                    ledPin.Write(true);
                    Thread.Sleep(blinkDelay);
                    ledPin.Write(false);
                    Thread.Sleep(blinkDelay);
                }
                else
                {
                    ledPin.Write(false);
                    Thread.Sleep(blinkDelay);
                    ledPin.Write(true);
                    Thread.Sleep(blinkDelay);
                }
            }
        }

        public byte[] SanitizeForUTF8(byte[] byteData)
        {
            if (byteData == null)
                return null;

            if (byteData.Length <= 0)
                return null;
                             
            for (int i = 0; i < byteData.Length; i++)
			{
                if (byteData[i] == 10 || byteData[i] == 13)
                    continue;                                

                if (byteData[i] < 32 || byteData[i] > 127)
                    byteData[i] = 32;                
			}
            return byteData;
        }

        public byte[] SanitizedForValidHexBytes(byte[] byteData)
        {
            bool invalidByteDetected = false;
            if (byteData == null)
                return null;

            if (byteData.Length <= 0)
                return null;

            for (int i = 0; i < byteData.Length; i++)
            {
                if (byteData[i] >= 48 && byteData[i] <= 57)
                    continue;
                else if (byteData[i] >= 65 && byteData[i] <= 70)
                    continue;
                else if (byteData[i] >= 97 && byteData[i] <= 102)
                    continue;
                else
                {
                    invalidByteDetected = true;
                    break;
                }
            }

            if (invalidByteDetected == true)
                return null;
            else
                return byteData;
        }

        public byte[] SanitizePointSixSerialNoByteArray(byte[] byteData)
        {
            bool invalidSerial = false;
            if (byteData == null)
                return null;

            if (byteData.Length <= 0)
                return null;
            
            for (int i = 0; i < byteData.Length; i++)
            {
                if (byteData[i] >= 48 && byteData[i] <= 57)
                    continue;
                else if (byteData[i] >= 65 && byteData[i] <= 90)
                    continue;
                else
                {
                    invalidSerial = true;
                    break;
                }                    
            }            

            if (invalidSerial == true)            
                return null;            
            else            
                return byteData;
        }

        public bool SendCmdToUART(SerialPort sp, string cmd, string expectedReply, int waitPeriodBeforeCheckingForReply)
        {
            byte[] uartResp = null;

            //Set SMS to text mode only
            WriteToUART(sp, cmd, true);
            Thread.Sleep(waitPeriodBeforeCheckingForReply);
            uartResp = SanitizeForUTF8(ReadUART(sp));
            if (UARTMessageContain(uartResp, expectedReply) == true)
                return true;
            else
                return false;
        }

        public int GetTimeSpanSeconds(TimeSpan ts)
        {
            int totalSeconds = 0;
            totalSeconds = (ts.Hours *3600) + (ts.Minutes * 60) + ts.Seconds;
            return totalSeconds;
        }

        public string GetGPRMCDecimal(string latitudeOrLongitude, bool isLatitude)
        {
            int sign = 0;
            if (isLatitude)
            {
                //if format is wrong
                if (latitudeOrLongitude.Length != 10)
                    return null;

                //Get sign
                if (latitudeOrLongitude[9] == 'N')
                    sign = 1;
                else if (latitudeOrLongitude[9] == 'S')
                    sign = -1;

                //Get degree
                double degree = double.Parse(latitudeOrLongitude.Substring(0, 2).Trim());
                //Get minutes
                double min = double.Parse(latitudeOrLongitude.Substring(2, 7).Trim());
                //Get decimal value
                double latDecimal = degree + (min / 60);

                return (latDecimal * sign).ToString();
            }
            else
            {
                //if format is wrong
                if (latitudeOrLongitude.Length != 11)
                    return null;

                //Get sign
                if (latitudeOrLongitude[10] == 'E')
                    sign = 1;
                else if (latitudeOrLongitude[10] == 'W')
                    sign = -1;

                //Get degree
                double degree = double.Parse(latitudeOrLongitude.Substring(0, 3).Trim());
                //Get minutes
                double min = double.Parse(latitudeOrLongitude.Substring(3, 7).Trim());
                //Get decimal value
                double longDecimal = degree + (min / 60);

                return (longDecimal * sign).ToString();
            }
        }
        
        public string[] ByteArrayToStringLines(byte[] byteArray)
        {            
            string gsmMsg = new string(Encoding.UTF8.GetChars(byteArray));
            string[] lines = gsmMsg.Split('\n');
            return lines;
        }

        public string GetSubscriberPhoneNumber(SerialPort sp)
        {
            byte[] uartResp = null;

            WriteToUART(sp, "AT+CNUM", true);
            Thread.Sleep(500);
            uartResp = SanitizeForUTF8(ReadUART(sp));

            if (uartResp == null)
                return null;

            string[] msg = ByteArrayToStringLines(uartResp);
            if (msg.Length > 0)
            {
                foreach (var line in msg)
                {
                    //check for "+CNUM" response                    
                    if (line.IndexOf("+CNUM:") != -1)
                    {
                        StringBuilder sb = new StringBuilder(line.Split(',')[1].ToString().Trim());
                        sb.Replace("+","").Replace('\"',' ');
                        string cnum = sb.ToString().Trim();

                        uartResp = null;
                        msg = null;
                        return cnum;
                    }
                }                                                          
            }

            uartResp = null;
            msg = null;
            return null;
        }
        
        public string ByteArrayToHexString(byte[] byteArray)
        {
            return ByteArr.ToHex(byteArray);
        }

        public byte[] HexStringToByteArray(string hexString)
        {
            return Hex.ToBytes(hexString);
        }

        public UInt64 ConvertToUInt64(string hex)
        {
            if (hex.Length <= 8)
            {
                return (UInt64)(UInt32)Convert.ToInt32(hex, 16);
            }
           
            hex = Str.PadLeft(hex, 16, '0');
            var high = hex.Substring(0, 8);
            var low = hex.Substring(8);

            return (ConvertToUInt64(high) << 32) | ConvertToUInt64(low);
        }

        
        ////////////////////////
        // SMS Helper Methods //
        ////////////////////////
       
        //public bool SetSMSToTextMode(SerialPort sp)
        //{
        //    if (SendCmdToUART(sp, "AT+CMGF=1", "OK", 2000) == true)
        //        return true;
        //    else
        //        return false;
        //}

        // //Send SMS messages
        //public  bool SendSMSMessage(SerialPort sp, string phoneNumber, string message)
        //{            
        //    if (SendCmdToUART(sp, "AT+CMGS=" + "\"" + phoneNumber + "\"", ">", 2000) == false)
        //        return false;

        //    //Send the message
        //    byte[] uartResp = null;
        //    WriteToUART(sp, message, false);
        //    WriteCTRL_Z(sp);
        //    Thread.Sleep(12000);
        //    uartResp = SanitizeForUTF8(ReadUART(sp));
        //    if (UARTMessageContain(uartResp, "OK") == false)
        //        return false;

        //    return true;
        //}


        ///////////////////////////
        // TCP/IP Helper Methods //
        ///////////////////////////
        public bool SetPhoneFullFunctionality(SerialPort sp)
        {
            if (SendCmdToUART(sp, "AT+CFUN=1", "OK", 2000) == true)
                return true;
            else
                return false;
        }

        public bool IsSIMReady(SerialPort sp)
        {
            if (SendCmdToUART(sp, "AT+CPIN?", "+CPIN: READY", 2000) == true)
                return true;
            else
                return false;
        }

        public bool SetSingleTCPSession(SerialPort sp)
        {           
            if (SendCmdToUART(sp, "AT+QIMUX=0", "OK", 2000) == true)
                return true;
            else
                return false;
        }

        public bool SetAPNConfiguration(SerialPort sp, string apnName, string apnUserName, string apnPassword)
        {
            string cmd = "AT+QIREGAPP=" + "\"" + apnName + "\"" + ",\"" + apnUserName + "\"" + ",\"" + apnPassword + "\"";
            if (SendCmdToUART(sp, cmd, "OK", 2000) == true)
                return true;
            else
                return false;
        }

        public bool RegisteredAPN(SerialPort sp)
        {
            if (SendCmdToUART(sp, "AT+QIREGAPP", "OK", 2000) == true)
                return true;
            else
                return false;
        }

        public bool OpenTCPSession(SerialPort sp, string hostIP, string portNumber)
        {
            string cmd = "AT+QIOPEN=\"TCP\"," + "\"" + hostIP + "\"" + ",\"" + portNumber + "\"";
            if (SendCmdToUART(sp, cmd, "CONNECT OK", 5000) == true)
            {
                return true;
            }           
            else
            {
                return false;
            }
        }

        public bool SendMsgOverTCPConnection(SerialPort sp, string messageToSend)
        {
            byte[] uartResp = null;
            if (SendCmdToUART(sp, "AT+QISEND", ">", 500) == true)
            {
                WriteToUART(sp, messageToSend, true);
                WriteCTRL_Z(sp);
                Thread.Sleep(500);
                uartResp = SanitizeForUTF8(ReadUART(sp));
                if (UARTMessageContain(uartResp, "SEND OK") == false)
                    return false;
                else
                    return true;
            }
            else
            {
                return false;
            }
        }

        //public bool SendMsgOverTCPConnection(SerialPort sp, byte[] messageToSend)
        //{
        //    byte[] uartResp = null;
        //    if (SendCmdToUART(sp, "AT+QISEND=" + messageToSend.Length, ">", 500) == true)
        //    {
        //        WriteToUART(sp, messageToSend, false);
        //        Thread.Sleep(500);
        //        uartResp = SanitizeForUTF8(ReadUART(sp));
        //        if (UARTMessageContain(uartResp, "SEND OK") == false)
        //            return false;
        //        else
        //            return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}

        //public bool SendMsgOverTCPConnection(SerialPort sp, byte[] messageToSend, string ackMsg, int waitForAckMsgTimeout)
        //{
        //    byte[] uartResp = null;
        //    if (SendCmdToUART(sp, "AT+QISEND", ">", 500) == true)
        //    {
        //        WriteToUART(sp, messageToSend, false);
        //        Thread.Sleep(500);
        //        uartResp = SanitizeForUTF8(ReadUART(sp));
        //        if (UARTMessageContain(uartResp, "SEND OK") == false)
        //        {
        //            return false;
        //        }
        //        else
        //        {
        //            //Check for server ack msg response, and return the package here
        //            Thread.Sleep(waitForAckMsgTimeout);
        //            uartResp = SanitizeForUTF8(ReadUART(sp));
        //            if (UARTMessageContain(uartResp, ackMsg) == false)
        //                return false;
        //            else
        //                return true;
        //        }
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}

        //public bool SendMsgOverTCPConnection(SerialPort sp, string messageToSend, string ackMsg, int waitForAckMsgTimeout)
        //{
        //    byte[] uartResp = null;
        //    if (SendCmdToUART(sp, "AT+QISEND", ">", 500) == true)
        //    {
        //        WriteToUART(sp, messageToSend, true);
        //        WriteCTRL_Z(sp);
        //        Thread.Sleep(500);
        //        uartResp = SanitizeForUTF8(ReadUART(sp));
        //        if (UARTMessageContain(uartResp, "SEND OK") == false)
        //        {
        //            return false;
        //        }
        //        else
        //        {
        //            //Check for server ack msg response, and return the package here
        //            Thread.Sleep(waitForAckMsgTimeout);
        //            uartResp = SanitizeForUTF8(ReadUART(sp));
        //            if (UARTMessageContain(uartResp, ackMsg) == false)
        //                return false;
        //            else
        //                return true;
        //        }
        //    }
        //    else
        //    {
        //        return false;
        //    }


        //}
       
        public HUB_RESPONSE SendMsgOverTCPConnectionToHub(SerialPort sp, byte[] messageToSend, int waitForHubAcknowlegementTimeout)
        {
            byte[] uartResp = null;
            
            if (SendCmdToUART(sp, "AT+QISEND=" + messageToSend.Length, ">", 500) == true)
            {
                WriteToUART(sp, messageToSend, false);
                Thread.Sleep(500);
                uartResp = SanitizeForUTF8(ReadUART(sp));
                
                if (UARTMessageContain(uartResp, "SEND OK") == false)
                {
                    return new HUB_RESPONSE(HUB_RESPONSE_CODE.SEND_FAILED, null);
                }
                else
                {
                    //Check for hub msg response, and deal with it here
                    Thread.Sleep(waitForHubAcknowlegementTimeout);
                    uartResp = SanitizeForUTF8(ReadUART(sp));
                    
                    //Check to see if HUB response with "NOK", if so the package must be ignore because it is not valid to the HUB
                    if (UARTMessageContain(uartResp, "NOK") == true)
                        return new HUB_RESPONSE(HUB_RESPONSE_CODE.NOT_OK, null);
                    
                    //Check to see if HOK present, if so Check to see if any command from HUB.                 
                    if (UARTMessageContain(uartResp, "HOK") == true)
                    {
                        //Check for HUB commands
                        if (UARTMessageContain(uartResp, "REQ_SETTING") == true)
                        {
                            return new HUB_RESPONSE(HUB_RESPONSE_CODE.OK_WITH_REQUEST_SETTING, uartResp);
                        }
                        else if (UARTMessageContain(uartResp, "SET_SETTING") == true)
                        {
                            return new HUB_RESPONSE(HUB_RESPONSE_CODE.OK_WITH_SET_SETTING, uartResp);
                        }
                        else
                        {
                            return new HUB_RESPONSE(HUB_RESPONSE_CODE.OK_WITH_NO_ACTION_COMMAND, uartResp);
                        }         
                    }
                    else //no response at all from the HUB
                    {
                        return new HUB_RESPONSE(HUB_RESPONSE_CODE.NO_RESPONSE, null);
                    }
          
                }
            }
            else
            {
                return new HUB_RESPONSE(HUB_RESPONSE_CODE.SEND_FAILED, null);
            }
        }

        public bool CloseTCPSession(SerialPort sp)
        {
            if (SendCmdToUART(sp, "AT+QICLOSE", "CLOSE OK", 2000) == true)
                return true;
            else
                return false;
        }
        
        
    }
}
